"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
exports.__esModule = true;
var bcrypt = require("bcryptjs");
var express = require("express");
var crypto = require("crypto");
var ex_1 = require("./ex");
var GoogleAuth = require("google-auth-library");
exports.GOOGLE_CLIENT_ID = '249166985359-52s651k4st6onss6ehvb3sb9779j7m9s.apps.googleusercontent.com';
exports.login = express.Router();
exports.signup = express.Router();
var auth = new GoogleAuth;
exports.client = new auth.OAuth2(exports.GOOGLE_CLIENT_ID, '', '');
/**
 *  Crypter
 */
var Crypter = (function () {
    function Crypter() {
    }
    Crypter.hash = function (pw) {
        return new Promise(function (resolve, reject) {
            var hashed = bcrypt.hash(pw, 14);
            if (hashed) {
                resolve(hashed);
            }
            else {
                reject(new Error('Error hashing password'));
            }
        });
    };
    Crypter.check_pw = function (pw, user) {
        return new Promise(function (resolve) {
            resolve(bcrypt.compare(pw, user.password));
        });
    };
    Crypter.check_csrf = function (req, res, next) {
        var _csrfheader = 'X-XSRF-TOKEN';
        var _csrftoken = 'XSRF-TOKEN';
        if (req.header(_csrfheader) && req.header(_csrfheader) === req.cookies[_csrftoken]) {
            next();
        }
        else {
            res.sendStatus(401);
        }
    };
    Crypter.check_google_token = function (req, res, next) {
        var _this = this;
        var token = req.query.at;
        if (!token) {
            res.sendStatus(401);
            return;
        }
        exports.client.verifyIdToken(token, exports.GOOGLE_CLIENT_ID, function (e, signin) { return __awaiter(_this, void 0, void 0, function () {
            var payload, sub, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (e) {
                            res.sendStatus(401);
                            return [2 /*return*/];
                        }
                        payload = signin.getPayload();
                        if (!(payload.aud === exports.GOOGLE_CLIENT_ID &&
                            (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com'))) return [3 /*break*/, 2];
                        sub = payload.sub;
                        return [4 /*yield*/, ex_1.usersDB.collection('users').findOne({ _id: sub })];
                    case 1:
                        user = _a.sent();
                        if (user) {
                            res.locals.at = sub;
                            next();
                        }
                        else {
                            res.sendStatus(401);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        res.sendStatus(401);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    Crypter.create_csrf_token = function () {
        return Promise.resolve(crypto.randomBytes(64).toString('base64')).then(function (result) { return result; });
    };
    return Crypter;
}());
exports.Crypter = Crypter;
exports.login.post('/', Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var token;
    return __generator(this, function (_a) {
        token = req.body.auth;
        if (!token) {
            res.json({ type: 'error' });
            return [2 /*return*/];
        }
        exports.client.verifyIdToken(token, exports.GOOGLE_CLIENT_ID, function (e, signin) { return __awaiter(_this, void 0, void 0, function () {
            var payload, sub, user, posts, liked, err_1, canlike;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (e) {
                            res.json({ type: 'error' });
                            return [2 /*return*/];
                        }
                        payload = signin.getPayload();
                        if (!(payload.aud === exports.GOOGLE_CLIENT_ID && (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com'))) return [3 /*break*/, 9];
                        sub = payload.sub;
                        return [4 /*yield*/, ex_1.usersDB.collection('users').findOne({ _id: sub })];
                    case 1:
                        user = _a.sent();
                        if (!!user) return [3 /*break*/, 2];
                        res.json({ type: 'register' });
                        return [3 /*break*/, 8];
                    case 2: return [4 /*yield*/, ex_1.postsDb.collection('ads').find({ user_id: sub }).project({ user_id: 0 }).toArray()];
                    case 3:
                        posts = _a.sent();
                        liked = [];
                        _a.label = 4;
                    case 4:
                        _a.trys.push([4, 6, , 7]);
                        return [4 /*yield*/, ex_1.postsDb.collection('ads').find({ _id: { $in: user.liked.map(function (p) { return p._id; }) } })
                                .project({ user_id: 0 }).toArray()];
                    case 5:
                        liked = _a.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        err_1 = _a.sent();
                        console.log(err_1);
                        liked = [];
                        return [3 /*break*/, 7];
                    case 7:
                        canlike = user.canlike.n === 1 || (user.canlike.n === 2 && user.canlike.t < Date.now());
                        res.json({ type: 'success', username: user.username, posts: posts, liked: liked,
                            canpost: user.canpost.t < Date.now(), canlike: canlike });
                        _a.label = 8;
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        res.json({ type: 'error' });
                        _a.label = 10;
                    case 10: return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
exports.signup.post('/', Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var token, username;
    return __generator(this, function (_a) {
        token = req.body.auth;
        username = req.body.username;
        exports.client.verifyIdToken(token, exports.GOOGLE_CLIENT_ID, function (e, signin) { return __awaiter(_this, void 0, void 0, function () {
            var payload, sub, insertion, user, posts, liked;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (e) {
                            res.json({ type: 'error' });
                            return [2 /*return*/];
                        }
                        payload = signin.getPayload();
                        if (!(payload.aud === exports.GOOGLE_CLIENT_ID && (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com'))) return [3 /*break*/, 5];
                        sub = payload.sub;
                        return [4 /*yield*/, ex_1.usersDB.collection('users').insert({ _id: sub, username: username, reg: Date.now(), role: 2, liked: [],
                                posts: [], canpost: { t: 1 }, canlike: { n: 2, t: 1 } })];
                    case 1:
                        insertion = _a.sent();
                        return [4 /*yield*/, ex_1.usersDB.collection('users').findOne({ _id: sub })];
                    case 2:
                        user = _a.sent();
                        return [4 /*yield*/, ex_1.postsDb.collection('ads').find({ user_id: sub }).project({ user_id: 0 }).toArray()];
                    case 3:
                        posts = _a.sent();
                        return [4 /*yield*/, ex_1.postsDb.collection('ads').find({ _id: { $in: user.liked.map(function (p) { return p._id; }) } })
                                .project({ user_id: 0 }).toArray()];
                    case 4:
                        liked = _a.sent();
                        res.json({ type: 'success', username: user.username, posts: posts, liked: liked, canlike: true, canpost: true });
                        return [3 /*break*/, 6];
                    case 5:
                        res.json({ type: 'error' });
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
