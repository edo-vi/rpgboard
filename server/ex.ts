import * as express from 'express';
import * as path from 'path';
import * as passport from 'passport';
import * as cookieparser from 'cookie-parser';
import * as bodyparser from 'body-parser';
import { isNumber } from 'util';
import { ObjectID } from 'bson';
import * as mongo from 'mongodb';
import * as sanitize from 'mongo-sanitize';
import * as compression from 'compression';


import { client, Crypter, GOOGLE_CLIENT_ID, login, signup } from './registration';
import { Server } from 'ws';

export const ONEDAY = 1000 * 60 * 60 * 24;
/**
 * Csrf protection setup
 */

const app = express();


app.use(bodyparser.json());
app.use(cookieparser());

app.use(compression());
/**
 * View engine setup
 */

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(express.static(path.join(__dirname, '..', 'dist')));
/**
 * Passport setup
 */
app.use(passport.initialize());

app.disable('x-powered-by');

const prod = false;

/**
 * Routes
 */
if (prod) {
    app.get('/', (req, res) => {
        // Csrf token generated when '/' is called
        if (req.cookies['XSRF-TOKEN']) {res.render('layout-prod2'); return; }
        Crypter.create_csrf_token().then(result => {
            res.render('layout-prod', {csrf: result});
        });
    });
} else {
    app.get('/', (req, res) => {
    // Csrf token generated when '/' is called
        if (req.cookies['XSRF-TOKEN']) {res.render('layout2'); return; }
        Crypter.create_csrf_token().then(result => {
        res.render('layout', {csrf: result});
        });
    });
}
app.get('/getgames', Crypter.check_csrf, async(req: any, res: any) => {
    const cat = req.query.c || null;
    const data = await gamesDB.collection('ext').find({categories: cat, owners: {$gt: 700}})
        .project({name: 1, owners: 1, ratings: 1, avg: 1}).sort({owners: -1}).toArray();
    res.json(data);
});

app.get('/getgamesnames', Crypter.check_csrf, async(req, res) => {
    const data = await gamesDB.collection('namelist').find().map(x => x._id).toArray();
    res.send(data);
});

app.get('/getposts', Crypter.check_csrf, async(req, res) => {
    const loc = req.query.l || null;
    const data = await postsDb.collection('ads').find({location: loc}).project({user_id: 0}).sort({timestamp: -1}).toArray();
    res.json(data);
});

app.get('/getaccountinfo', Crypter.check_csrf, Crypter.check_google_token, async(req, res) => {
    const p_id = req.query.pid || null;
    const post = await postsDb.collection('ads').findOne({_id: new ObjectID(p_id)});
    let user = await usersDB.collection('users').find({_id: post.user_id}).project({canlike: 0, canpost: 0}).toArray();
    user = user[0];
    const posts = await postsDb.collection('ads').find({user_id: user._id}).project({user_id: 0}).toArray();
    let liked = [];
    try {
        liked = await postsDb.collection('ads').find({_id: {$in: user.liked.map(p => p._id)}}).project({user_id: 0}).toArray();
    } catch (err) {
        console.log(err);
        liked = [];
    }
    res.json({type: 'success', payload: {username: user.username, reg: user.reg, posts: posts, liked: liked}});
});
app.post('/addpost', Crypter.check_csrf, Crypter.check_google_token, async(req: any, res: any) => {
    for (const key in req.body) {
        if (!/^(game|location|description|user_username)$/.test(key) ) {
            res.sendStatus(400);
            return;
        }
    }
    // check input
    if (req.body.description.length > 200 || req.body.description.length === 0
        || req.body.game.length === 0 || req.body.location.length === 0) {
            res.sendStatus(400); return;
    }
    // sanitize input
    const game = sanitize(req.body.game);
    const location = sanitize(req.body.location);
    const description = split_in_chunks(sanitize(req.body.description));
    const user_id = sanitize(res.locals.at);
    const username = sanitize(req.body.user_username);
    const timestamp = Date.now();
    const post = {timestamp: timestamp, game: game, location: location,
        description: description, user_id: user_id, user_username: username};
    const insertion = await postsDb.collection('ads').insert(post);
    const user = await usersDB.collection('users').findOne({_id: user_id});
    if (user['canpost']['t'] < Date.now()) {
        const inserteduser = await usersDB.collection('users').findOneAndUpdate({_id: user_id}, {$addToSet: {posts: insertion.ops[0]._id},
            $set: {canpost: {t: Date.now() + ONEDAY}}}, {returnOriginal: false });
        res.json({type: 'success', payload: insertion.ops[0]});
    } else {
        res.json({type: 'error', description: `Cannot post until ${new Date(user['canpost']['t'])}`});
    }
});


app.post('/likepost', Crypter.check_csrf, Crypter.check_google_token, async(req: any, res: any) => {
    const user_id = sanitize(res.locals.at);
    const post_id = sanitize(req.body['post_id']);
    const user = await usersDB.collection('users').findOne({_id: user_id});
    if (user['posts'].some(id => id.equals(new ObjectID(post_id))) ||
        (user['liked'].length > 0 && user['liked'].map(p => p._id).some(id => id.equals(new ObjectID(post_id))))) {
            res.sendStatus(400);
            return;
    }
    const now = Date.now();
    switch (user['canlike']['n']) {
        case 2:
            const insertion = await usersDB.collection('users').findOneAndUpdate({_id: user_id},
                {$addToSet: {liked: {_id: new ObjectID(post_id), timestamp: now}}, $set: {canlike: {n: 1, t: now + ONEDAY}}});
            if (insertion.ok === 1) {
                res.json({type: 'success'});
            } else {
                res.json({type: 'error'});
            }
            return;
        case 1:
            let insertion1 = null;
            if (user['canlike']['t'] < now) {
                insertion1 = await usersDB.collection('users').findOneAndUpdate({_id: user_id},
                {$addToSet: {liked: {_id: new ObjectID(post_id), timestamp: now}},
                    $set: {canlike: {n: 1, t: now + ONEDAY}}}, {returnOriginal: false });
            } else {
                insertion1 = await usersDB.collection('users').findOneAndUpdate({_id: user_id},
                {$addToSet: {liked: {_id: new ObjectID(post_id), timestamp: now}},
                    $inc: {'canlike.n': -1}}, {returnOriginal: false });
            }
            if (insertion1.ok === 1) {
                res.json({type: 'success'});
            } else {
                res.json({type: 'error'});
            }
            return;
        case 0:
            if (user['canlike']['t'] < now) {
                const insertion2 = await usersDB.collection('users').findOneAndUpdate({_id: user_id},
                {$addToSet: {liked: {_id: new ObjectID(post_id), timestamp: now}},
                    $set: {canlike: {n: 1, t: now + ONEDAY}}}, {returnOriginal: false });
            } else {
            res.json({type: 'error', description: `Cannot like until ${new Date(user['canlike']['t'])}`});
            }
    }
});

app.get('/username', Crypter.check_csrf, async(req: any, res: any) => {
    const user = await usersDB.collection('users').findOne({username: sanitize(req.query.user)});
    if (!user) {res.json({type: 'success'}); }
    else {res.json({type: 'error'}); }
});

// use it only if you want to generate and send csrf tokens with another server call
app.get('/csrf', async(req, res) => {
    const result = await Crypter.create_csrf_token();
    res.json({'csrf': result});
});

export let usersDB: any;
export let postsDb: any;
export let gamesDB: any;

export interface User {
        _id: ObjectID;
        username: string;
        password: string;
        cats: Array<Object>;
}

const httpserver = app.listen(4200, 'localhost', () => {
    const {address, port} = httpserver.address();
    console.log(`listening on port: ${port} on route: ${address}`);
    /**
    * Mongo setup
    */
    const MongoClient = mongo.MongoClient;
    MongoClient.connect('mongodb://localhost:6666/users').then(database => usersDB = database);
    MongoClient.connect('mongodb://localhost:6666/bgg').then(database => gamesDB = database);
    MongoClient.connect('mongodb://localhost:6666/ads').then(db => postsDb = db);
});

const wsserver: Server = new Server({server: httpserver});

wsserver.on('connection', ws => {
    ws.on('message', (msg) => {
        msg = parse(msg);
        if (msg['type'] === 'like') {
            updated_likes(msg['payload']).then(r => {
                ws.send(JSON.stringify(r));
            });
            return;
        } else if (msg['type'] === 'add') {
            updated_posts(msg['payload']).then(r => {
                ws.send(JSON.stringify(r));
            });
        } else {
            ws.send(JSON.stringify({status: 'error'}));
        }
    });
    ws.on('close', () => {

    });
});


/**
 * Routers
 */
app.use('/login', login);
app.use('/signup', signup);

app.get('*', (req, res) => {
    if (req.originalUrl === '/trova' || req.originalUrl === '/games') {
            res.redirect(301, '/?next=' + encodeURIComponent(req.originalUrl));
    } else {
        res.redirect(301, '/');
    }
});

function split_in_chunks(s: string) {
    const chunks = [];
    for (const c of s.split(' ')) {
        if (c.length > 30) {
            const minchunk = [];
            const num = Math.floor(c.length / 30);
            for (let i = 0, m = 0; i < num; i++, m += 30) {
                minchunk.push(c.slice(m, m + 30));
            }
            for (const n of minchunk) {
                chunks.push(n);
            }
        } else {
            chunks.push(c);
        }
    }
    return chunks.join(' ');
}

function parse (s: any) {
    return JSON.parse(s.toString());
}

function updated_likes(jwt: string) {
    return new Promise(resolve => {
        if (!jwt) {resolve({type: 'returned-like', status: 'error'}); return; }
        client.verifyIdToken(jwt, GOOGLE_CLIENT_ID, async(e, signin) => {
            if (e) {resolve({type: 'returned-like', status: 'error'}); }
            const payload = signin.getPayload();
            if (payload.aud === GOOGLE_CLIENT_ID &&
                (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com')) {
                    const sub = payload.sub;
                    let liked = await usersDB.collection('users').findOne({_id: sub}, {_id: 0, liked: 1, canlike: 1});
                    const canlike = !(liked.canlike.n === 0 && liked.canlike.t > Date.now());
                    liked = liked.liked.sort((a, b) => b.timestamp - a.timestamp);
                    const data = await postsDb.collection('ads').find({_id: liked[0]._id}).project({user_id: 0})
                        .sort({timestamp: -1}).toArray();
                    resolve({type: 'returned-like', status: 'success', payload: data[0], canlike: canlike});
            } else {resolve({type: 'returned-like', status: 'error'}); }
        });
    });
}

function updated_posts(jwt: string) {
    return new Promise(resolve => {
        if (!jwt) {resolve({type: 'returned-add', status: 'error'}); return; }
        client.verifyIdToken(jwt, GOOGLE_CLIENT_ID, async(e, signin) => {
            if (e) {resolve({type: 'returned-add', status: 'error'}); }
            const payload = signin.getPayload();
            if (payload.aud === GOOGLE_CLIENT_ID &&
                (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com')) {
                    const sub = payload.sub;
                    const data = await postsDb.collection('ads').find({user_id: sub}).project({user_id: 0}).sort({timestamp: -1})
                        .toArray();
                    const user = await usersDB.collection('users').findOne({_id: sub});
                    const canpost = user.canpost.t < Date.now();
                    resolve({type: 'returned-add', status: 'success', payload: data[0], canpost: canpost});
            } else {resolve({type: 'returned-add', status: 'error'}); }
        });
    });
}
