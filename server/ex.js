"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
exports.__esModule = true;
var express = require("express");
var path = require("path");
var passport = require("passport");
var cookieparser = require("cookie-parser");
var bodyparser = require("body-parser");
var bson_1 = require("bson");
var mongo = require("mongodb");
var sanitize = require("mongo-sanitize");
var compression = require("compression");
var registration_1 = require("./registration");
var ws_1 = require("ws");
exports.ONEDAY = 1000 * 60 * 60 * 24;
/**
 * Csrf protection setup
 */
var app = express();
app.use(bodyparser.json());
app.use(cookieparser());
app.use(compression());
/**
 * View engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, '..', 'dist')));
/**
 * Passport setup
 */
app.use(passport.initialize());
app.disable('x-powered-by');
var prod = false;
/**
 * Routes
 */
if (prod) {
    app.get('/', function (req, res) {
        // Csrf token generated when '/' is called
        if (req.cookies['XSRF-TOKEN']) {
            res.render('layout-prod2');
            return;
        }
        registration_1.Crypter.create_csrf_token().then(function (result) {
            res.render('layout-prod', { csrf: result });
        });
    });
}
else {
    app.get('/', function (req, res) {
        // Csrf token generated when '/' is called
        if (req.cookies['XSRF-TOKEN']) {
            res.render('layout2');
            return;
        }
        registration_1.Crypter.create_csrf_token().then(function (result) {
            res.render('layout', { csrf: result });
        });
    });
}
app.get('/getgames', registration_1.Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var cat, data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                cat = req.query.c || null;
                return [4 /*yield*/, exports.gamesDB.collection('ext').find({ categories: cat, owners: { $gt: 700 } })
                        .project({ name: 1, owners: 1, ratings: 1, avg: 1 }).sort({ owners: -1 }).toArray()];
            case 1:
                data = _a.sent();
                res.json(data);
                return [2 /*return*/];
        }
    });
}); });
app.get('/getgamesnames', registration_1.Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, exports.gamesDB.collection('namelist').find().map(function (x) { return x._id; }).toArray()];
            case 1:
                data = _a.sent();
                res.send(data);
                return [2 /*return*/];
        }
    });
}); });
app.get('/getposts', registration_1.Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var loc, data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                loc = req.query.l || null;
                return [4 /*yield*/, exports.postsDb.collection('ads').find({ location: loc }).project({ user_id: 0 }).sort({ timestamp: -1 }).toArray()];
            case 1:
                data = _a.sent();
                res.json(data);
                return [2 /*return*/];
        }
    });
}); });
app.get('/getaccountinfo', registration_1.Crypter.check_csrf, registration_1.Crypter.check_google_token, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var p_id, post, user, posts, liked, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                p_id = req.query.pid || null;
                return [4 /*yield*/, exports.postsDb.collection('ads').findOne({ _id: new bson_1.ObjectID(p_id) })];
            case 1:
                post = _a.sent();
                return [4 /*yield*/, exports.usersDB.collection('users').find({ _id: post.user_id }).project({ canlike: 0, canpost: 0 }).toArray()];
            case 2:
                user = _a.sent();
                user = user[0];
                return [4 /*yield*/, exports.postsDb.collection('ads').find({ user_id: user._id }).project({ user_id: 0 }).toArray()];
            case 3:
                posts = _a.sent();
                liked = [];
                _a.label = 4;
            case 4:
                _a.trys.push([4, 6, , 7]);
                return [4 /*yield*/, exports.postsDb.collection('ads').find({ _id: { $in: user.liked.map(function (p) { return p._id; }) } }).project({ user_id: 0 }).toArray()];
            case 5:
                liked = _a.sent();
                return [3 /*break*/, 7];
            case 6:
                err_1 = _a.sent();
                console.log(err_1);
                liked = [];
                return [3 /*break*/, 7];
            case 7:
                res.json({ type: 'success', payload: { username: user.username, reg: user.reg, posts: posts, liked: liked } });
                return [2 /*return*/];
        }
    });
}); });
app.post('/addpost', registration_1.Crypter.check_csrf, registration_1.Crypter.check_google_token, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var key, game, location, description, user_id, username, timestamp, post, insertion, user, inserteduser;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                for (key in req.body) {
                    if (!/^(game|location|description|user_username)$/.test(key)) {
                        res.sendStatus(400);
                        return [2 /*return*/];
                    }
                }
                // check input
                if (req.body.description.length > 200 || req.body.description.length === 0
                    || req.body.game.length === 0 || req.body.location.length === 0) {
                    res.sendStatus(400);
                    return [2 /*return*/];
                }
                game = sanitize(req.body.game);
                location = sanitize(req.body.location);
                description = split_in_chunks(sanitize(req.body.description));
                user_id = sanitize(res.locals.at);
                username = sanitize(req.body.user_username);
                timestamp = Date.now();
                post = { timestamp: timestamp, game: game, location: location,
                    description: description, user_id: user_id, user_username: username };
                return [4 /*yield*/, exports.postsDb.collection('ads').insert(post)];
            case 1:
                insertion = _a.sent();
                return [4 /*yield*/, exports.usersDB.collection('users').findOne({ _id: user_id })];
            case 2:
                user = _a.sent();
                if (!(user['canpost']['t'] < Date.now())) return [3 /*break*/, 4];
                return [4 /*yield*/, exports.usersDB.collection('users').findOneAndUpdate({ _id: user_id }, { $addToSet: { posts: insertion.ops[0]._id },
                        $set: { canpost: { t: Date.now() + exports.ONEDAY } } }, { returnOriginal: false })];
            case 3:
                inserteduser = _a.sent();
                res.json({ type: 'success', payload: insertion.ops[0] });
                return [3 /*break*/, 5];
            case 4:
                res.json({ type: 'error', description: "Cannot post until " + new Date(user['canpost']['t']) });
                _a.label = 5;
            case 5: return [2 /*return*/];
        }
    });
}); });
app.post('/likepost', registration_1.Crypter.check_csrf, registration_1.Crypter.check_google_token, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var user_id, post_id, user, now, _a, insertion, insertion1, insertion2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                user_id = sanitize(res.locals.at);
                post_id = sanitize(req.body['post_id']);
                return [4 /*yield*/, exports.usersDB.collection('users').findOne({ _id: user_id })];
            case 1:
                user = _b.sent();
                if (user['posts'].some(function (id) { return id.equals(new bson_1.ObjectID(post_id)); }) ||
                    (user['liked'].length > 0 && user['liked'].map(function (p) { return p._id; }).some(function (id) { return id.equals(new bson_1.ObjectID(post_id)); }))) {
                    res.sendStatus(400);
                    return [2 /*return*/];
                }
                now = Date.now();
                _a = user['canlike']['n'];
                switch (_a) {
                    case 2: return [3 /*break*/, 2];
                    case 1: return [3 /*break*/, 4];
                    case 0: return [3 /*break*/, 9];
                }
                return [3 /*break*/, 12];
            case 2: return [4 /*yield*/, exports.usersDB.collection('users').findOneAndUpdate({ _id: user_id }, { $addToSet: { liked: { _id: new bson_1.ObjectID(post_id), timestamp: now } }, $set: { canlike: { n: 1, t: now + exports.ONEDAY } } })];
            case 3:
                insertion = _b.sent();
                if (insertion.ok === 1) {
                    res.json({ type: 'success' });
                }
                else {
                    res.json({ type: 'error' });
                }
                return [2 /*return*/];
            case 4:
                insertion1 = null;
                if (!(user['canlike']['t'] < now)) return [3 /*break*/, 6];
                return [4 /*yield*/, exports.usersDB.collection('users').findOneAndUpdate({ _id: user_id }, { $addToSet: { liked: { _id: new bson_1.ObjectID(post_id), timestamp: now } },
                        $set: { canlike: { n: 1, t: now + exports.ONEDAY } } }, { returnOriginal: false })];
            case 5:
                insertion1 = _b.sent();
                return [3 /*break*/, 8];
            case 6: return [4 /*yield*/, exports.usersDB.collection('users').findOneAndUpdate({ _id: user_id }, { $addToSet: { liked: { _id: new bson_1.ObjectID(post_id), timestamp: now } },
                    $inc: { 'canlike.n': -1 } }, { returnOriginal: false })];
            case 7:
                insertion1 = _b.sent();
                _b.label = 8;
            case 8:
                if (insertion1.ok === 1) {
                    res.json({ type: 'success' });
                }
                else {
                    res.json({ type: 'error' });
                }
                return [2 /*return*/];
            case 9:
                if (!(user['canlike']['t'] < now)) return [3 /*break*/, 11];
                return [4 /*yield*/, exports.usersDB.collection('users').findOneAndUpdate({ _id: user_id }, { $addToSet: { liked: { _id: new bson_1.ObjectID(post_id), timestamp: now } },
                        $set: { canlike: { n: 1, t: now + exports.ONEDAY } } }, { returnOriginal: false })];
            case 10:
                insertion2 = _b.sent();
                return [3 /*break*/, 12];
            case 11:
                res.json({ type: 'error', description: "Cannot like until " + new Date(user['canlike']['t']) });
                _b.label = 12;
            case 12: return [2 /*return*/];
        }
    });
}); });
app.get('/username', registration_1.Crypter.check_csrf, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, exports.usersDB.collection('users').findOne({ username: sanitize(req.query.user) })];
            case 1:
                user = _a.sent();
                if (!user) {
                    res.json({ type: 'success' });
                }
                else {
                    res.json({ type: 'error' });
                }
                return [2 /*return*/];
        }
    });
}); });
// use it only if you want to generate and send csrf tokens with another server call
app.get('/csrf', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, registration_1.Crypter.create_csrf_token()];
            case 1:
                result = _a.sent();
                res.json({ 'csrf': result });
                return [2 /*return*/];
        }
    });
}); });
var httpserver = app.listen(4200, 'localhost', function () {
    var _a = httpserver.address(), address = _a.address, port = _a.port;
    console.log("listening on port: " + port + " on route: " + address);
    /**
    * Mongo setup
    */
    var MongoClient = mongo.MongoClient;
    MongoClient.connect('mongodb://localhost:6666/users').then(function (database) { return exports.usersDB = database; });
    MongoClient.connect('mongodb://localhost:6666/bgg').then(function (database) { return exports.gamesDB = database; });
    MongoClient.connect('mongodb://localhost:6666/ads').then(function (db) { return exports.postsDb = db; });
});
var wsserver = new ws_1.Server({ server: httpserver });
wsserver.on('connection', function (ws) {
    ws.on('message', function (msg) {
        msg = parse(msg);
        if (msg['type'] === 'like') {
            updated_likes(msg['payload']).then(function (r) {
                ws.send(JSON.stringify(r));
            });
            return;
        }
        else if (msg['type'] === 'add') {
            updated_posts(msg['payload']).then(function (r) {
                ws.send(JSON.stringify(r));
            });
        }
        else {
            ws.send(JSON.stringify({ status: 'error' }));
        }
    });
    ws.on('close', function () {
    });
});
/**
 * Routers
 */
app.use('/login', registration_1.login);
app.use('/signup', registration_1.signup);
app.get('*', function (req, res) {
    if (req.originalUrl === '/trova' || req.originalUrl === '/games') {
        res.redirect(301, '/?next=' + encodeURIComponent(req.originalUrl));
    }
    else {
        res.redirect(301, '/');
    }
});
function split_in_chunks(s) {
    var chunks = [];
    for (var _i = 0, _a = s.split(' '); _i < _a.length; _i++) {
        var c = _a[_i];
        if (c.length > 30) {
            var minchunk = [];
            var num = Math.floor(c.length / 30);
            for (var i = 0, m = 0; i < num; i++, m += 30) {
                minchunk.push(c.slice(m, m + 30));
            }
            for (var _b = 0, minchunk_1 = minchunk; _b < minchunk_1.length; _b++) {
                var n = minchunk_1[_b];
                chunks.push(n);
            }
        }
        else {
            chunks.push(c);
        }
    }
    return chunks.join(' ');
}
function parse(s) {
    return JSON.parse(s.toString());
}
function updated_likes(jwt) {
    var _this = this;
    return new Promise(function (resolve) {
        if (!jwt) {
            resolve({ type: 'returned-like', status: 'error' });
            return;
        }
        registration_1.client.verifyIdToken(jwt, registration_1.GOOGLE_CLIENT_ID, function (e, signin) { return __awaiter(_this, void 0, void 0, function () {
            var payload, sub, liked, canlike, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (e) {
                            resolve({ type: 'returned-like', status: 'error' });
                        }
                        payload = signin.getPayload();
                        if (!(payload.aud === registration_1.GOOGLE_CLIENT_ID &&
                            (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com'))) return [3 /*break*/, 3];
                        sub = payload.sub;
                        return [4 /*yield*/, exports.usersDB.collection('users').findOne({ _id: sub }, { _id: 0, liked: 1, canlike: 1 })];
                    case 1:
                        liked = _a.sent();
                        canlike = !(liked.canlike.n === 0 && liked.canlike.t > Date.now());
                        liked = liked.liked.sort(function (a, b) { return b.timestamp - a.timestamp; });
                        return [4 /*yield*/, exports.postsDb.collection('ads').find({ _id: liked[0]._id }).project({ user_id: 0 })
                                .sort({ timestamp: -1 }).toArray()];
                    case 2:
                        data = _a.sent();
                        resolve({ type: 'returned-like', status: 'success', payload: data[0], canlike: canlike });
                        return [3 /*break*/, 4];
                    case 3:
                        resolve({ type: 'returned-like', status: 'error' });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    });
}
function updated_posts(jwt) {
    var _this = this;
    return new Promise(function (resolve) {
        if (!jwt) {
            resolve({ type: 'returned-add', status: 'error' });
            return;
        }
        registration_1.client.verifyIdToken(jwt, registration_1.GOOGLE_CLIENT_ID, function (e, signin) { return __awaiter(_this, void 0, void 0, function () {
            var payload, sub, data, user, canpost;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (e) {
                            resolve({ type: 'returned-add', status: 'error' });
                        }
                        payload = signin.getPayload();
                        if (!(payload.aud === registration_1.GOOGLE_CLIENT_ID &&
                            (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com'))) return [3 /*break*/, 3];
                        sub = payload.sub;
                        return [4 /*yield*/, exports.postsDb.collection('ads').find({ user_id: sub }).project({ user_id: 0 }).sort({ timestamp: -1 })
                                .toArray()];
                    case 1:
                        data = _a.sent();
                        return [4 /*yield*/, exports.usersDB.collection('users').findOne({ _id: sub })];
                    case 2:
                        user = _a.sent();
                        canpost = user.canpost.t < Date.now();
                        resolve({ type: 'returned-add', status: 'success', payload: data[0], canpost: canpost });
                        return [3 /*break*/, 4];
                    case 3:
                        resolve({ type: 'returned-add', status: 'error' });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    });
}
