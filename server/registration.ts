import * as bcrypt from 'bcryptjs';
import * as express from 'express';

import * as crypto from 'crypto';

import { usersDB, User, postsDb } from './ex';

import * as GoogleAuth from 'google-auth-library';


export const GOOGLE_CLIENT_ID = '249166985359-52s651k4st6onss6ehvb3sb9779j7m9s.apps.googleusercontent.com';
export let login = express.Router();
export let signup = express.Router();

const auth = new GoogleAuth;
export const client = new auth.OAuth2(GOOGLE_CLIENT_ID, '', '');


/**
 *  Crypter
 */
export class Crypter {
    static hash(pw: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const hashed = bcrypt.hash(pw, 14);
            if (hashed) {resolve(hashed); } else {
                reject(new Error('Error hashing password'));
            }
        });
    }
    static check_pw(pw: string, user: User): Promise<boolean> {
        return new Promise((resolve) => {
            resolve(bcrypt.compare(pw, user.password));
        });
    }
    static check_csrf(req: any, res: any, next: any): any {
        const _csrfheader = 'X-XSRF-TOKEN';
        const _csrftoken = 'XSRF-TOKEN';
        if (req.header(_csrfheader) && req.header(_csrfheader) === req.cookies[_csrftoken]) {next(); } else {
            res.sendStatus(401);
        }
    }
    static check_google_token(req: any, res: any, next: any) {
        const token = req.query.at;
        if (!token) {res.sendStatus(401); return; }
        client.verifyIdToken(token, GOOGLE_CLIENT_ID, async(e, signin) => {
            if (e) {res.sendStatus(401); return; }
            const payload = signin.getPayload();
            if (payload.aud === GOOGLE_CLIENT_ID &&
                (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com')) {
                    const sub = payload.sub;
                    const user = await usersDB.collection('users').findOne({_id: sub});
                    if (user) { res.locals.at = sub; next(); } else {
                        res.sendStatus(401);
                    }
            } else {
                res.sendStatus(401);
            }
        });
    }
    static create_csrf_token(): Promise<string> {
        return Promise.resolve(crypto.randomBytes(64).toString('base64')).then((result: string) => result);
    }
}

login.post('/', Crypter.check_csrf, async(req, res) => {
    const token = req.body.auth;
    if (!token) {res.json({type: 'error'}); return; }
    client.verifyIdToken(token, GOOGLE_CLIENT_ID, async(e, signin) => {
        if (e) {res.json({type: 'error'}); return; }
        const payload = signin.getPayload();
        if (payload.aud === GOOGLE_CLIENT_ID && (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com')) {
            const sub = payload.sub;
            const user = await usersDB.collection('users').findOne({_id: sub});
            if (!user) {res.json({type: 'register'}); } else {
                const posts = await postsDb.collection('ads').find({user_id: sub}).project({user_id: 0}).toArray();
                let liked = [];
                try {
                    liked = await postsDb.collection('ads').find({_id: {$in: user.liked.map(p => p._id)}})
                    .project({user_id: 0}).toArray();
                } catch (err) {
                    console.log(err);
                    liked = [];
                }
                const canlike = user.canlike.n === 1 || (user.canlike.n === 2 && user.canlike.t < Date.now());
                res.json({type: 'success', username: user.username, posts: posts, liked: liked,
                    canpost: user.canpost.t < Date.now(), canlike: canlike});
            }
        } else {
            res.json({type: 'error'});
        }
    });
});

signup.post('/', Crypter.check_csrf, async(req, res) => {
    const token = req.body.auth;
    const username = req.body.username;
    client.verifyIdToken(token, GOOGLE_CLIENT_ID, async(e, signin) => {
        if (e) {res.json({type: 'error'}); return; }
        const payload = signin.getPayload();
        if (payload.aud === GOOGLE_CLIENT_ID && (payload.iss === 'accounts.google.com' || payload.iss === 'https://accounts.google.com')) {
            const sub = payload.sub;
            const insertion = await usersDB.collection('users').insert({_id: sub, username: username, reg: Date.now(), role: 2, liked: []
            , posts: [], canpost: {t: 1}, canlike: {n: 2, t: 1}});
            const user = await usersDB.collection('users').findOne({_id: sub});
            const posts = await postsDb.collection('ads').find({user_id: sub}).project({user_id: 0}).toArray();
            const liked = await postsDb.collection('ads').find({_id: {$in: user.liked.map(p => p._id)}})
                .project({user_id: 0}).toArray();
            res.json({type: 'success', username: user.username, posts: posts, liked: liked, canlike: true, canpost: true});
        } else {
            res.json({type: 'error'});
        }
    });
    /*
    const user = await usersDB.collection('users').findOne({username: sanitize(req.body.username)});

    if (user) {res.json({type: 'error', description: 'Username already taken'}); } else {
        const hashed: string = await Crypter.hash(sanitize(req.body.password));
        const insertion = await usersDB.collection('users').insertOne({username: sanitize(req.body.username), password: hashed});
        const user = await usersDB.collection('users').findOne({username: sanitize(req.body.username)});

        const payload = {_id: user._id};
        const token = jwt.sign(payload, opt.secretOrKey);
        res.json({user: {jwt: token, username: user.username}, cats: []});
    }*/
});

