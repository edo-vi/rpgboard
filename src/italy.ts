

import { Region } from './app/components/trova/trova.component';

export const regions: Region[] = [
    {
        name: "Abruzzo",
        hovering: false,
        counties: [{name: 'Chieti', hovering: false}, {name: 'L\'Aquila', hovering: false},
            {name: 'Pescara', hovering: false}, {name: 'Teramo', hovering: false} ]
    },
    {
        name: "Basilicata",
        hovering: false,
        counties: [{name: 'Matera', hovering: false}, {name: 'Potenza', hovering: false}]
    },
    {
        name: "Calabria",
        hovering: false,
        counties: [{name: 'Catanzaro', hovering: false}, {name: 'Cosenza', hovering: false},
            {name: 'Crotone', hovering: false}, {name: 'Reggio Calabria', hovering: false},
            {name: 'Vibo Valentia', hovering: false}]
    },
    {
        name: "Campania",
        hovering: false,
        counties: [{name: 'Avellino', hovering: false}, {name: 'Benevento', hovering: false},
            {name: 'Caserta', hovering: false}, {name: 'Napoli', hovering: false},
            {name: 'Salerno', hovering: false} ]
    },
    {
        name: "Emilia-Romagna",
        hovering: false,
        counties: [{name: 'Bologna', hovering: false}, {name: 'Ferrara', hovering: false},
            {name: 'Forlì-Cesena', hovering: false}, {name: 'Modena', hovering: false},
            {name: 'Parma', hovering: false}, {name: 'Piacenza', hovering: false},
            {name: 'Ravenna', hovering: false}, {name: 'Reggio Emilia', hovering: false},
            {name: 'Rimini', hovering: false}]
    },
    {
        name: "Friuli",
        hovering: false,
        counties: [{name: 'Gorizia', hovering: false}, {name: 'Pordenone', hovering: false},
            {name: 'Trieste', hovering: false}, {name: 'Udine', hovering: false}]
    },
    {
        name: "Lazio",
        hovering: false,
        counties: [{name: 'Frosinone', hovering: false}, {name: 'Latina', hovering: false},
            {name: 'Rieti', hovering: false}, {name: 'Roma', hovering: false},
            {name: 'Viterbo', hovering: false}]
    },
    {
        name: "Liguria",
        hovering: false,
        counties: [{name: 'Genova', hovering: false}, {name: 'Imperia', hovering: false},
            {name: 'La Spezia', hovering: false}, {name: 'Savona', hovering: false}]
    },
    {
        name: "Lombardia",
        hovering: false,
        counties: [{name: 'Bergamo', hovering: false}, {name: 'Brescia', hovering: false},
            {name: 'Como', hovering: false}, {name: 'Cremona', hovering: false},
            {name: 'Lecco', hovering: false}, {name: 'Lodi', hovering: false},
            {name: 'Mantova', hovering: false}, {name: 'Milano', hovering: false},
            {name: 'Monza e Brianza', hovering: false}, {name: 'Pavia', hovering: false},
            {name: 'Sondrio', hovering: false}, {name: 'Varese', hovering: false}]
    },
    {
        name: "Marche",
        hovering: false,
        counties: [{name: 'Ancona', hovering: false}, {name: 'Ascoli Piceno', hovering: false},
            {name: 'Fermo', hovering: false}, {name: 'Macerata', hovering: false},
            {name: 'Pesaro e Urbino', hovering: false}]
    },
    {
        name: "Molise",
        hovering: false,
        counties: [{name: 'Campobasso', hovering: false}, {name: 'Isernia', hovering: false}]
    },
    {
        name: "Piemonte",
        hovering: false,
        counties: [{name: 'Alessandria', hovering: false}, {name: 'Asti', hovering: false},
            {name: 'Biella', hovering: false}, {name: 'Cuneo', hovering: false},
            {name: 'Novara', hovering: false}, {name: 'Torino', hovering: false},
            {name: 'Verbano Cusio Ossola', hovering: false}, {name: 'Vercelli', hovering: false}]
    },
    {
        name: "Puglia",
        hovering: false,
        counties: [{name: 'Bari', hovering: false}, {name: 'Barletta-Andria-Trani', hovering: false},
            {name: 'Brindisi', hovering: false}, {name: 'Lecce', hovering: false},
            {name: 'Foggia', hovering: false}, {name: 'Taranto', hovering: false}]
    },
    {
        name: "Sardegna",
        hovering: false,
        counties: [{name: 'Cagliari', hovering: false}, {name: 'Carbonia-Iglesias', hovering: false},
            {name: 'Medio Campidano', hovering: false}, {name: 'Nuoro', hovering: false},
            {name: 'Ogliastra', hovering: false}, {name: 'Olbia-Tempio', hovering: false},
            {name: 'Oristano', hovering: false}, {name: 'Sassari', hovering: false}]
    },
    {
        name: "Sicilia",
        hovering: false,
        counties: [{name: 'Agrigento', hovering: false}, {name: 'Caltanissetta', hovering: false},
            {name: 'Catania', hovering: false}, {name: 'Enna', hovering: false},
            {name: 'Messina', hovering: false}, {name: 'Palermo', hovering: false},
            {name: 'Ragusa', hovering: false}, {name: 'Siracusa', hovering: false},
            {name: 'Trapani', hovering: false} ]
    },
    {
        name: "Toscana",
        hovering: false,
        counties: [{name: 'Arezzo', hovering: false}, {name: 'Firenze', hovering: false},
            {name: 'Grosseto', hovering: false}, {name: 'Livorno', hovering: false},
            {name: 'Lucca', hovering: false}, {name: 'Massa e Carrara', hovering: false},
            {name: 'Pisa', hovering: false}, {name: 'Pistoia', hovering: false},
            {name: 'Prato', hovering: false}, {name: 'Siena', hovering: false}]
    },
    {
        name: "Trentino",
        hovering: false,
        counties: [{name: 'Bolzano', hovering: false}, {name:'Trento', hovering: false}]
    },
    {
        name: "Umbria",
        hovering: false,
        counties: [{name: 'Perugia', hovering: false}, {name: 'Terni', hovering: false} ]
    },
    {
        name: "Valle d'Aosta",
        hovering: false,
        counties: [{name: 'Aosta', hovering: false}]
    },
    {
        name: "Veneto",
        hovering: false,
        counties: [{name: 'Belluno', hovering: false}, {name: 'Padova', hovering: false},
            {name: 'Rovigo', hovering: false}, {name: 'Treviso', hovering: false},
            {name: 'Venezia', hovering: false}, {name: 'Verona', hovering: false},
            {name: 'Vicenza', hovering: false}]
    }
];

