import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { PostsService } from '../services/posts.service';
import { Post, ADDEDPOST, ADDINGPOST, GOTPOSTS, GETPOSTS, LIKED } from './postsreducer';
import 'rxjs/add/operator/mapTo';
import { Store } from '@ngrx/store';
import { OKAYTOUPDATELIKED, UPDATEUSERSPOSTS } from './login_reducer';
import { UpdaterService } from '../services/updater.service';


@Injectable()
export class PostsEffects {
    @Effect() getPosts$ = this.actions$.ofType(GETPOSTS)
        .map(toPayload)
        .switchMap(p => this.service.get_posts(p))
        .map(ads => ({type: GOTPOSTS, payload: ads}));
    @Effect() addPost$ = this.actions$.ofType(ADDINGPOST).map(toPayload)
        .switchMap((p) => {
            return this.service.add_post(p)
                .filter(resp => resp.type === 'success')
                .map(resp => ({type: ADDEDPOST, payload: resp.payload}));
        });
    @Effect() addLike$ = this.actions$.ofType(LIKED).map(toPayload).withLatestFrom(this.store.select('login').pluck('jwt'))
        .switchMap((r) => {
            const post_id: any = r[0];
            const jwt: any = r[1];
            return this.service.like_post(post_id, jwt)
                .filter(resp => resp.type === 'success')
                .map(res => ({type: OKAYTOUPDATELIKED, payload: res.canlike}));
        });
    @Effect({dispatch: false}) updaterObs$ = this.actions$.ofType(OKAYTOUPDATELIKED).map(act => {
            this.updater.ask_for_added_like();
        });
    @Effect({dispatch: false}) updaterObs2$ = this.actions$.ofType(ADDEDPOST).map(act => {
            this.updater.ask_for_added_post();
        });
    constructor(protected actions$: Actions, protected service: PostsService,
                protected store: Store<Post[]>, protected updater: UpdaterService) {}
}

function split_in_chunks(s: string) {
    const chunks = [];
    for (const c of s.split(' ')) {
        if (c.length > 30) {
            const minchunk = [];
            const num = Math.floor(c.length / 30);
            for (let i = 0, m = 0; i < num; i++, m += 30) {
                minchunk.push(c.slice(m, m + 30));
            }
            for (const n of minchunk) {
                chunks.push(n);
            }
        } else {
            chunks.push(c);
        }
    }
    return chunks.join(' ');
}
