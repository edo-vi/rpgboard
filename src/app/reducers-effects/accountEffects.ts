import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { GETACCOUNTINFO, GOTACCOUNTINFO } from './accountreducer';
import { Http } from '@angular/http';

@Injectable()
export class AccountEffects {
    @Effect() getAccountInfo$ = this.actions$.ofType(GETACCOUNTINFO).map(toPayload).switchMap(({pid, jwt}) => {
        return this.http.get(`/getaccountinfo?pid=${pid}&at=${jwt}`).map(x => x.json()).filter(x => x.type === 'success')
            .map((a: any) => ({type: GOTACCOUNTINFO, payload: a['payload']}));
    });
    constructor(protected actions$: Actions, protected http: Http) {}
}
