import { Action } from '@ngrx/store';
import { isNullOrUndefined } from 'util';
import { REMOVECITY } from './locationreducer';

export const GETPOSTS = '[POSTS] Getting Posts List';
export const GOTPOSTS = '[POSTS] Got Posts List';
export const ADDINGPOST = '[POSTS] Adding New Post';
export const ADDEDPOST = '[POSTS] Successfully Added New Post';
export const ORDERNEWEST = '[POSTS] Order Posts By Newest';
export const ORDEROLDEST = '[POSTS] Order Posts By Oldest';
export const LIKED = '[POST] Liked Post';

export interface Post {
    _id: any;
    game: string;
    location: string;
    description: string;
    timestamp: number;
    user_id?: string;
    user_username?: string;
}

export function posts_list_reducer(state: Post[] = [], action: Action) {
    switch (action.type) {
        case GOTPOSTS:
            return action.payload;
        case ADDEDPOST:
            return [action.payload, ...state];
        case ORDERNEWEST:
            return state.slice().sort((a, b) => b.timestamp - a.timestamp);
        case ORDEROLDEST:
            return state.slice().sort((a, b) => a.timestamp - b.timestamp);
        default:
            return state;
    }
}

export function posts_order_reducer(state = 'newest', action: Action) {
    switch (action.type) {
        case ORDERNEWEST:
            return 'newest';
        case ORDEROLDEST:
            return 'oldest';
        case REMOVECITY:
            return 'newest'; // todo check this
        default:
            return state;
    }
}

