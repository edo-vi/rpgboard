import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { MOVETOGAME, SELECTEDCOUNTY } from './locationreducer';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/do';
import { MOVETOBOARD } from './boardreducer';
import { Router } from '@angular/router';

@Injectable()
export class LocationEffects {
    /*@Effect({dispatch: false}) combineLocationWithGame$ = this.actions$.ofType(SELECTEDCOUNTY)
        .withLatestFrom(this.store.select('chosen_game').pluck('name')).do(([, game]) => {
        if (game === null) {
            this.router.navigateByUrl('/games');
        } else {
            this.router.navigateByUrl('/board');
        }
        });*/
    constructor(protected store: Store<any>, protected actions$: Actions,
                protected router: Router) {}
}
