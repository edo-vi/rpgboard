import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { LOOKED, SEARCHED } from './searchreducer';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import { SearchService } from '../services/search.service';
import { LEFTCITYCOMP } from './locationreducer';

@Injectable()
export class SearchEffects {
    @Effect() searchForTerm$ = this.actions$.ofType(SEARCHED).map(toPayload)
        .map(payload => {
            if (payload === '') {return {type: LEFTCITYCOMP}; }
            return {type: LOOKED, payload: this.service.look_for_counties(payload)};
        });

    constructor(protected store: Store<string>, protected actions$: Actions,
                protected service: SearchService) {}
}
