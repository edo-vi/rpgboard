import { Action } from '@ngrx/store';
import { regions } from '../../italy';
import { Region } from '../components/trova/trova.component';


export const SELECTEDREGION = '[LOCATION] Selected Region';
export const SELECTEDCOUNTY = '[LOCATION] Selected County';
export const SELECTEDCITY = '[LOCATION] Selected City';
export const LEFTCITYCOMP = '[LOCATION] Left CityComponent';
export const WENTBACK = '[LOCATION] Went Back';
export const REMOVECITY = '[LOCATION] Removed Selected City';
export const MOVETOGAME = '[LOCATION] Chosen Location But No Game';


export const initialLocationState: Region[] = regions;

export function chosen_location_reducer(state = [null, null], action: Action) {
    switch (action.type) {
        case SELECTEDCOUNTY:
            return [state[0], action.payload];
        case SELECTEDREGION:
            return [action.payload.name, null];
        case REMOVECITY:
            return [null, null];
        default:
            return state;
    }
}

