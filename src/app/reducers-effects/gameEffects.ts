import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/zip';
import {
    FINDGAMES, FINDGAMESNAMES, FOUNDGAMES, GOTNAMES, MOVETOCITIES, SELECTEDCATEGORY,
    SELECTEDGAME
} from './gamereducer';
import { GameService } from '../services/game.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GameEffects {
    @Effect() getHot$ = this.actions$.ofType(SELECTEDCATEGORY).map(toPayload)
        .switchMap(({en, }) => this.service.get_games(en))
        .map(res => ({type: FOUNDGAMES, payload: res}));
    @Effect() getNames$ = this.actions$.ofType(FINDGAMESNAMES)
        .zip(Observable.of(1), (a, b) => a) // limit to 1
        .withLatestFrom(this.store.select('games_names_list'), (a, b) => b)
        .filter((b: any[]) => b.length === 0)
        .switchMap(action => this.service.get_names())
        .map(result => {
            return {type: GOTNAMES, payload: result};
        });
    @Effect({dispatch: false}) goToTrova$ = this.actions$.ofType(SELECTEDGAME)
        .do(x => this.router.navigateByUrl('/trova'));
    constructor(protected actions$: Actions, protected store: Store<any>,
                protected service: GameService, protected router: Router) {}
}
