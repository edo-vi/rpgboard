import { Action } from '@ngrx/store';

export const LOOKED = '[SEARCH] Looked for input'
export const SEARCHED = '[SEARCH] Searched for term';
export function search_reducer(state: string = '', action: Action) {
    switch (action.type) {
        case SEARCHED:
            return action.payload;
        default:
            return state;
    }
}
