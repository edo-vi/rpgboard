import { Action } from '@ngrx/store';

export interface Game {
    name: string;
    hovering?: boolean;
}
export const SELECTEDGAME = '[GAME] Chosen Game';
export const REMOVEGAME = '[GAME] Remove Game';
export const MOVETOCITIES = '[GAME] Chosen Game But No Location';
export const FINDGAMES = '[GAME] Populate Game List';
export const FOUNDGAMES = '[GAME] Found Games';
export const FINDGAMESNAMES = '[GAME] Get Names';
export const GOTNAMES = '[GAMES] Got Names';
export const SELECTEDCATEGORY = '[Games] Selected Category';
export const CLEARCATEGORY = '[Games] Cleared Category';

const initialGameState: string = null;

export function display_games_reducer(state = [], action: Action) {
    switch (action.type) {
        case FOUNDGAMES:
            return action.payload;
        default:
            return state;
    }
}

export function chosen_game_reducer(state: string = null, action: Action) {
    switch (action.type) {
        case SELECTEDGAME:
            return action.payload;
        case REMOVEGAME:
            return initialGameState;
        default:
            return state;
    }
}

export function games_names_list_reducer(state: string[] = [], action: Action) {
    switch (action.type) {
        case GOTNAMES:
            return action.payload;
        default:
            return state;
    }
}

export function chosen_category_reducer(state = {en: null, it: null}, action: Action) {
    switch (action.type) {
        case SELECTEDCATEGORY:
            return action.payload;
        case CLEARCATEGORY:
            return {en: null, it: null};
        default:
            return state;
    }
}
