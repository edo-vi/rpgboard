import { Action } from '@ngrx/store';

export const SHOWFORM = '[POSTFORM] Show Form';
export const HIDFORM = '[POSTFORM] Hid Form';

export function show_form_reducer(state = false, action: Action) {
    switch (action.type) {
        case SHOWFORM:
            return true;
        case HIDFORM:
            return false;
        default:
            return state;
    }
}
