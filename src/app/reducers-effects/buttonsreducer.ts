
import { Action, combineReducers } from '@ngrx/store';
import { FOUNDGAMES } from './gamereducer';

export const GAMESPERPAGE = 15;
export const BUTTONSTODISPLAY = 7;

export const GOTOPAGE = '[BUTTONS] Go to Page N';
export const NEXT = '[BUTTONS] Clicked Next';
export const PREV = '[BUTTONS] Clicked Prev';

export function total_pages_reducers(state: number = 1, action: Action) {
    switch (action.type) {
        case FOUNDGAMES:
            return Math.ceil(action.payload.length / GAMESPERPAGE);
        default:
            return state;
    }
}

export function current_page_reducer(state: number = 1, action: Action) {
    switch (action.type) {
        case GOTOPAGE:
            return action.payload;
        case FOUNDGAMES:
            return 1;
        default:
            return state;
    }
}

export function portion_reducer(state = {c: 1 , max: 1}, action: Action) {
    switch (action.type) {
        case FOUNDGAMES:
            return {c: 1, max: Math.ceil(Math.ceil(action.payload.length / GAMESPERPAGE) / BUTTONSTODISPLAY)};
        case NEXT:
            return {c: state.c + 1, max: state.max};
        case PREV:
            return {c: state.c - 1, max: state.max};
        default:
            return state;
    }
}

export function button_state_reducer(reducer) {
    return combineReducers(reducer);
}

export const button_state = {
    'total_pages': total_pages_reducers,
    'current_page': current_page_reducer,
    'portion': portion_reducer
};
