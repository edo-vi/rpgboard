import { Action } from '@ngrx/store';

export const GETCSRF = '[CSRF] Get CSRF Token';
export const GOTXSRF = '[CSRF] Got CSRF Token';

export function xsrf_reducer(state: string = null, action: Action) {
    switch (action.type) {
        case GOTXSRF:
            return action.payload;
        default:
            return state;
    }
}
