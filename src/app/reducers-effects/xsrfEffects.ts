import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
// import { GETCSRF, GOTXSRF } from './xsrfReducer';
// import { AuthService } from '../services/auth.service';

import { Observable } from 'rxjs/Observable';
import * as Cookie from 'js-cookie';
import { isNullOrUndefined } from 'util';
import { GOTXSRF } from './xsrfReducer';

export const LOAD = '[DB] Load';

@Injectable()
export class XsrfEffects {
    isThereTheElement$: Observable<any|null> = Observable.of(document.getElementById('_csrf'));
    @Effect() loadCsrf$ = this.actions$.ofType(LOAD).withLatestFrom(this.isThereTheElement$, (a, b) => b)
        .map(element => {
            if (isNullOrUndefined(element)) {return {type: GOTXSRF, payload: Cookie.get('XSRF-TOKEN')}; } else {
                Cookie.set('XSRF-TOKEN', element.name);
                return {type: GOTXSRF, payload: element.name};
            }
        });
    constructor(private actions$: Actions) {}
}
