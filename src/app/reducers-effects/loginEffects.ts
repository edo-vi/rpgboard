
import { Actions, Effect, toPayload } from '@ngrx/effects';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/fromPromise';

import {
    DELETEDUSER, FAILEDREGISTRATION, LOADUSER, LOGGED, LOGIN, LOGOUT, REGISTER, REGISTERED,
    SAVEDUSER, SUCCESSFULREGISTRATION
} from '../reducers-effects/login_reducer';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AuthService, MockAuthService } from '../services/auth.service';
import { Action, Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../components/login/register/register.component';

const FIVEDAYSINMS = 432000000;

export const TIMESTAMPED = '[DB] Added Timestamp';
export const MUSTDELETE = '[DB] Must Remove Data';
export const OKAYTOLOAD = '[DB] Okay to Load Data';

@Injectable()
export class LoginEffects {
    constructor(protected actions$: Actions, protected auth: AuthService, protected store: Store<any>,
                protected router: Router, protected modal: NgbModal) {}
    @Effect() logUser$: Observable<Action> = this.actions$.ofType(LOGIN).map(toPayload).switchMap(payload => {
        return this.auth.login(payload)
            .filter(response => {
                return response.type !== 'error';
            }).map(response => {
                if (response.type !== 'register') {
                    return {type: LOGGED, payload: {user: {username: response.username, jwt: payload.auth,
                        posts: response.posts, liked: response.liked, canpost: response.canpost, canlike: response.canlike}}
                    };
                } else {
                   return {type: REGISTER};
                }
            });
    });
    @Effect() registerUser$: Observable<Action> = this.actions$.ofType(REGISTER).switchMap(x => {
        return this.modal.open(RegisterComponent).result
            .then(username => ({type: SUCCESSFULREGISTRATION, payload: username})
            )
            .catch(dismissed => ({type: FAILEDREGISTRATION}));
    });
    @Effect({dispatch: false}) failedRegistration$ = this.actions$.ofType(FAILEDREGISTRATION).do(() => this.auth.logout());
    @Effect() successfulRegistration$ = this.actions$.ofType(SUCCESSFULREGISTRATION).map(toPayload).switchMap((p) => {
        if (!p) {throw new Error('username must not be null or undefined'); }
        const token = this.auth.get_token();
        return this.auth.register({auth: token, username: p}).map((res) => {
            if (res.type === 'success') {
                return {type: REGISTERED, payload: {user: {username: res.username, jwt: token, posts: res.posts, liked: res.liked,
                canpost: res.canpost, canlike: res.canlike}}};
            }
        });
    }).catch((err) => {console.log(err); return Observable.of({type: FAILEDREGISTRATION}); });
    @Effect({dispatch: false}) logout$: Observable<any> = this.actions$.ofType(LOGOUT)
        .map(x => this.auth.logout()).do(x => {
            if (this.router.url === '/user') {
                this.router.navigateByUrl('/');
            }
    });
}
