import { Action } from '@ngrx/store';
import { Post } from './postsreducer';

export const LOGGED = '[Login] Logged';
export const LOGIN = '[Login] Log In';
export const LOGOUT = '[Login] Log Out';
export const REGISTER = '[Login] Register';
export const SUCCESSFULREGISTRATION = '[Login] Successful Registration';
export const FAILEDREGISTRATION = '[Login] Failed Registration';
export const REGISTERED = '[Login] Registered';
export const SAVEDUSER = '[DB] Saved User to DB';
export const DELETEDUSER = '[DB] Deleted User from Db';
export const LOADUSER = '[DB] Loaded User from Db';
export const OKAYTOUPDATELIKED = '[Login] Okay To Update Liked';
export const UPDATEDLIKED = '[Login] Updated Liked Posts';
export const UPDATEUSERSPOSTS = '[Login] Updated Users Posts';

export interface LoginStatus {
    isLogged: boolean;
    jwt: string;
    username: string;
    posts: Post[];
    liked: Post[];
}

export const initialState = {isLogged: false, jwt: '', username: '', posts: null, liked: null, canpost: false, canlike: false};

export function loginReducer(state: LoginStatus = initialState, action: Action) {
    switch (action.type) {
        case LOGGED:
            return {isLogged: true, jwt: action.payload.user.jwt , username: action.payload.user.username,
                posts: action.payload.user.posts, liked: action.payload.user.liked, canpost: action.payload.user.canpost,
            canlike: action.payload.user.canlike};
        case REGISTERED:
            return {isLogged: true, jwt: action.payload.user.jwt , username: action.payload.user.username,
                posts: action.payload.user.posts, liked: action.payload.user.liked, canpost: action.payload.user.canpost,
            canlike: action.payload.user.canlike};
        case UPDATEDLIKED:
            const narray = [...state.liked, action.payload.like];
            const canlike = action.payload.canlike;
            return {...state, liked: narray, canlike: canlike};
        case UPDATEUSERSPOSTS:
            const newarray = [...state.posts, action.payload.post];
            return {...state, posts: newarray, canpost: action.payload.canpost};
        case LOGOUT:
            return {isLogged: false, jwt: '', username: ''};
        case LOADUSER:
            return {isLogged: action.payload.isLogged, jwt: action.payload.value.jwt, username: action.payload.value.username};
        default:
            return state;
    }
}

