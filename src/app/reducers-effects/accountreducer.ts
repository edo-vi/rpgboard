import { Action } from '@ngrx/store';
import { Post } from './postsreducer';

export interface Account {
    username: string;
    reg: number;
    posts: Post[];
    liked: Post[];
}

export const GETACCOUNTINFO = '[ACCOUNT] Get Account Info';
export const GOTACCOUNTINFO = '[ACCOUNT] Got Account Info';

export function account_reducer(state: Account = null, action: Action) {
    switch (action.type) {
        case GOTACCOUNTINFO:
            return action.payload;
        default:
            return state;
    }
}
