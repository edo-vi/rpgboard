import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { combineReducers, StoreModule } from '@ngrx/store';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SearchformComponent } from './components/searchform/searchform.component';
import { environment } from '../environments/environment.prod';
import { compose } from '@ngrx/core';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './reducers-effects/searchEffects';
import { HttpModule } from '@angular/http';
import { routes } from './routes';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationEffects } from './reducers-effects/locationEffects';
import { GameEffects } from './reducers-effects/gameEffects';

import {
    chosen_category_reducer, chosen_game_reducer, display_games_reducer,
    games_names_list_reducer
} from './reducers-effects/gamereducer';
import {
    chosen_location_reducer
} from './reducers-effects/locationreducer';

import { SearchService } from './services/search.service';

import { GameService } from './services/game.service';
import { xsrf_reducer } from './reducers-effects/xsrfReducer';
import { XsrfEffects } from './reducers-effects/xsrfEffects';
import { LoginEffects } from './reducers-effects/loginEffects';
import { loginReducer } from './reducers-effects/login_reducer';
import { LocalStorageMediator } from './services/localstorage.mediator';
import { AuthService, MockAuthService } from './services/auth.service';
import { ToIsoDatePipe, TrovaComponent } from './components/trova/trova.component';
import { posts_list_reducer, posts_order_reducer } from './reducers-effects/postsreducer';
import { PostsEffects } from './reducers-effects/postsEffects';
import { PostsService } from './services/posts.service';
import { PostsResolver } from './services/postsResolver';
import { PostFormComponent } from './components/trova/post-form/post-form.component';
import { show_form_reducer } from './reducers-effects/showformreducer';
import { SidebarComponent } from './components/trova/sidebar/sidebar.component';
import { UserComponent } from './components/user/user.component';
import { LoginGuard, LoginGuardClientSide, PreventAccessToLoggedUsers } from './services/login.guard';
import { GamesComponent } from './components/games/games.component';
import { ButtonComponent } from './components/button/button.component';
import { button_state, button_state_reducer, current_page_reducer } from './reducers-effects/buttonsreducer';
import { GoogleAuthComponent } from './components/login/google-auth/google-auth.component';
import { UserPostComponent } from './components/user/user-post/user-post.component';
import { RegisterComponent } from './components/login/register/register.component';
import { TrovaPostComponent } from './components/trova/trova-post/trova-post.component';
import { UpdaterService } from './services/updater.service';
import { AccountComponent } from './components/user/account/account.component';
import { account_reducer } from './reducers-effects/accountreducer';
import { AccountEffects } from './reducers-effects/accountEffects';
import { JumbotronService } from './services/jumbotron.service';


/**
 * This because of AOT errors caused by ngrx store
 */
const reducers = {
    'posts_list': posts_list_reducer,
    'posts_order': posts_order_reducer,
    'login': loginReducer,
    'account': account_reducer,
    'games_list': display_games_reducer,
    'games_names_list': games_names_list_reducer,
    'chosen_game': chosen_game_reducer,
    'chosen_category': chosen_category_reducer,
    'button_state': button_state_reducer(button_state),
    'chosen_location': chosen_location_reducer,
    'xsrf': xsrf_reducer,
    'show_form': show_form_reducer,
};

const developmentReducer = compose(combineReducers)(reducers);
const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  }
  else {
    return developmentReducer(state, action);
  }
}
/********/

@NgModule({
    declarations: [
    DashboardComponent,
    SearchformComponent, HomeComponent, TrovaComponent, ToIsoDatePipe,
        PostFormComponent, SidebarComponent, UserComponent, GamesComponent, ButtonComponent, GoogleAuthComponent,
        UserPostComponent, RegisterComponent, TrovaPostComponent, AccountComponent
    ],
    imports: [
    BrowserModule, ReactiveFormsModule, HttpModule, FormsModule, BrowserAnimationsModule, routes,
        StoreModule.provideStore(reducer),
        EffectsModule.run(SearchEffects),
        EffectsModule.run(XsrfEffects),
        EffectsModule.run(LocationEffects),
        EffectsModule.run(GameEffects),
        EffectsModule.run(LoginEffects),
        EffectsModule.run(PostsEffects),
        EffectsModule.run(AccountEffects),
        StoreDevtoolsModule.instrumentStore(),
        NgbModule.forRoot()
    ],
    providers: [SearchService, GameService, LocalStorageMediator,
        {provide: AuthService, useClass: AuthService}, PostsService,
        PostsResolver, PreventAccessToLoggedUsers, LoginGuardClientSide, LoginGuard, UpdaterService, JumbotronService],
    entryComponents: [PostFormComponent, GoogleAuthComponent, RegisterComponent],
    bootstrap: [DashboardComponent]
})
export class AppModule { }

