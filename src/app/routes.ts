import { Route, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';

import { TrovaComponent } from './components/trova/trova.component';

import { UserComponent } from './components/user/user.component';
import { LoginGuardClientSide, PreventAccessToLoggedUsers } from './services/login.guard';

import { GamesComponent } from './components/games/games.component';
import { AccountComponent } from './components/user/account/account.component';

const routeList: Route[] = [
    {component: HomeComponent, path: ''},
    {component: TrovaComponent, path: 'trova'},
    {component: UserComponent, path: 'user', canActivate: [LoginGuardClientSide]},
    {component: AccountComponent, path: 'account/:username', canActivate: [LoginGuardClientSide]},
    {component: GamesComponent, path: 'games'}
];

export const routes = RouterModule.forRoot(routeList);
