import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Response } from '@angular/http';
import { Store } from '@ngrx/store';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(private auth: AuthService, protected store: Store<any>) {}
    islogged$: Observable<boolean> = this.store.select('login').pluck('isLogged');
    username: string;
    check_server_side(): Observable<Response> {
        return this.auth.test_token();
    }

    canActivate(): Observable<boolean> {
        return this.check_server_side().map(response => response.json()).map(res => {
            if (res.type === 'Success') {
                this.username = res.data.username;
                return true;
            }
            else return false;
        });
    }
}

@Injectable()
export class LoginGuardClientSide extends LoginGuard {
    canActivate(): Observable<boolean> {
        return this.islogged$;
    }
}

@Injectable()
export class PreventAccessToLoggedUsers extends LoginGuard {
    canActivate(): Observable<boolean> {
        return this.islogged$.map(x => !x);
    }
}
