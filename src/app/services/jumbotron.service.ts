import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { style, animate, transition, trigger, state } from '@angular/animations';

export const jumbotronanimation = trigger('bigsmall', [
    state('small', style({
        height: '1%',
        padding: '10px',
        backgroundColor: 'white'
    })),
    transition('big -> small', animate('300ms ease')),
    transition('small -> big', animate('300ms ease'))
]);


@Injectable()
export class JumbotronService {
    public state_ = new BehaviorSubject('big');
    constructor(protected router: Router) {
        this.router.events.filter((ev: any) => ev instanceof NavigationEnd)
            .pluck('url')
            .pairwise()
            .subscribe(([start, end]) => {
                if ((start === '/' || /^\/\?next.*?$/) && end !== '/') {
                    this.state_.next('small');
                } else if ((start !== '/' || /^\/\?next.*?$/) && end === '/') {
                    this.state_.next('big');
                }
        });
    }

}
