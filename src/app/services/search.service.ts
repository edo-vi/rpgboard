import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { initialLocationState, LEFTCITYCOMP } from '../reducers-effects/locationreducer';
import { County, Region } from '../components/trova/trova.component';

@Injectable()
export class SearchService {
    locations$: Observable<Region[]|County[]>;
    look_for_counties(input: string) {
        if (!input) {this.store.dispatch({type: LEFTCITYCOMP}); }
        else {
            return initialLocationState.map(elem => {
            return elem.counties;
            }).reduce((a, b) => a.concat(b) ).filter(elem => {
                const reg = new RegExp('^.*' + input + '.*$');
                return reg.test(elem.name.toLowerCase());
            });
        }
    }
    constructor(protected store: Store<any>) {
        this.locations$ = this.store.select('locations_list');
    }
}
