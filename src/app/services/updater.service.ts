import { Injectable } from '@angular/core';

import { UPDATEDLIKED, UPDATEUSERSPOSTS } from '../reducers-effects/login_reducer';
import { Store } from '@ngrx/store';

@Injectable()
export class UpdaterService {
    socket: WebSocket;
    addsocket: WebSocket;
    jwt: string;
    constructor(protected store: Store<any>) {
        this.store.select('login').pluck('jwt').subscribe((x: string) => this.jwt = x);
        this.socket = new WebSocket('ws://localhost:4200');
        this.socket.addEventListener('message', function (event) {
            const data = JSON.parse(event.data.toString());
            if (data.type === 'returned-like' ) {
                store.dispatch({type: UPDATEDLIKED, payload: {like: data.payload, canlike: data.canlike}});
                return;
            } else if (data.type === 'returned-add' ) {
                store.dispatch({type: UPDATEUSERSPOSTS, payload: {post: data.payload, canpost: data.canpost}});
                return;
            }
        });
    }
    ask_for_posts_update() {
        return this.socket.send(JSON.stringify({type: 'posts'}));
    }
    ask_for_added_like() {
        return this.socket.send(JSON.stringify({type: 'like', payload: this.jwt}));
    }
    ask_for_added_post() {
        return this.socket.send(JSON.stringify({type: 'add', payload: this.jwt}));
    }

}
