import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ADDEDPOST, Post } from '../reducers-effects/postsreducer';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/pluck';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { OKAYTOUPDATELIKED, UPDATEUSERSPOSTS } from '../reducers-effects/login_reducer';
import { UpdaterService } from './updater.service';

@Injectable()
export class PostsService {
    constructor(protected http: Http, protected store: Store<any>,
                protected actions$: Actions) {
    }
    get_posts(loc: string) {
        return this.http.get(`getposts?l=${loc}`)
            .map(res => res.json())
            .catch(err => {
                // console.log(err);
                return Observable.of(mockdata);
            });
    }
    add_post(formdata: {name: string, location: string, description: string}) {
        return Observable.of(1).withLatestFrom(this.store.select('login'), (a, b) => b).switchMap(({isLogged, jwt, username, }) => {
            formdata['user_username'] = username;
            return this.http.post(`addpost?at=${jwt}`, formdata)
                .map(res => res.json());
            }).catch(err => {
                // console.log(err);
                return Observable.of({type: 'error'});
        });
    }
    like_post(post_id: any, jwt: any) {
        return this.http.post(`likepost?at=${jwt}`, {post_id: post_id} ).map(x => x.json());
    }
}

const mockdata: Post[] = [
    {_id: 1, game: 'Magic: The Gathering', location: 'Verona', description: `
    precipitevolissimevo lmente precipitevolisssimevol menteprecipitevoli ssimevolmente
      3402775857`, user_id: 'uisahd87>&/(%/&',
    timestamp: 1501412543112},
    {_id: 2, timestamp: 1101412328716, location: 'Chieti', description:
        'Chiamatemi :) 2656494512', game: 'Android: Netrunner', user_id: 'uisahd87>&/(%/&'},
    {_id: 3, timestamp: 1333412324111, game: 'Dungeons & Dragons 6', location: 'Chieti',
    description: 'I wanna play', user_id: 'uisahd87>&/(%/&'},
    {_id: 4, game: 'Arboria', location: 'Torino', timestamp: 1285599328716,
        description: 'returned by mockdata', user_id: 'uisahd87>&/(%/&'}
    ];
