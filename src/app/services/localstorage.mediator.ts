import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LocalStorageMediator {
    localStorageSubject: Subject<string>;
    constructor() {
        this.localStorageSubject = new Subject();
    }
    static retrieve_jwt_token(key: string): string {
        return JSON.parse(localStorage.getItem(key)).jwt ? JSON.parse(localStorage.getItem(key)).jwt : '';
    }
    static retrieve_user_info(key: string = 'currentUser'): string {
        return JSON.parse(localStorage.getItem(key))
    }
    static set_jwt_token(key: string, payload: any) {
        localStorage.setItem(key, JSON.stringify(payload));
    };
    static remove_jwt_token(key: string) {
        localStorage.removeItem(key);
    }
    retrieve_username(): Promise<string> {
        return new Promise((resolve,reject) => {
            if (!localStorage.getItem('currentUser')) reject('No localstorage item');
            else resolve(JSON.parse(localStorage.getItem('currentUser')).username)
        })
    }
}
