import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorageMediator } from './localstorage.mediator';
import { Error } from 'tslint/lib/error';
import { LOGIN } from '../reducers-effects/login_reducer';
import { Store } from '@ngrx/store';

declare const gapi: any;

@Injectable()
export class AuthService {
    public googleAuth: any;
    public googleUser: any;
    constructor(protected http: Http, protected store: Store<any>) {
        gapi.load('auth2', () => {
            gapi.auth2.init({client_id: '249166985359-52s651k4st6onss6ehvb3sb9779j7m9s.apps.googleusercontent.com'})
                .then(i => this.googleAuth = i)
                .then(() => this.googleUser = this.googleAuth.currentUser.get())
                .then(() => this.store.dispatch({type: LOGIN, payload: {auth: this.googleUser.getAuthResponse().id_token}}));
        });
    }
    /****************************************
     * Token methods
     ****************************************/

    test_token(): Observable<Response> {
        let key = 'currentUser';
        let headers = new Headers({'Authorization': `JWT ${LocalStorageMediator.retrieve_jwt_token(key)}` });
        let options = new RequestOptions({ headers: headers });
        let body = {};
        return this.http.post(`/test`, body, options);
    }
    /****************************************
     * Login/signup methods
     ****************************************/
    login(data: any): Observable<any> {
        return this.http.post('/login', data).map(x => x.json()).catch(err => {
            throw new Error('Error authenticating');
        });
    }
    get_token() {
        return this.googleUser.getAuthResponse().id_token || null;
    }
    register(data: any): Observable<any> {
        return this.http.post('/signup', data).map(x => x.json());
    }
    logout(): void {
        this.googleAuth.signOut();
    }
}

@Injectable()
export class MockAuthService extends AuthService {
    login() {
        return Observable.of({user: {username: 'test', jwt: 'usaoipdja()7 3298479328.987 3264IQIUWIU'}});
    }
}
