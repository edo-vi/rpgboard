import { Resolve } from '@angular/router';
import { Post } from '../reducers-effects/postsreducer';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Injectable()
export class PostsResolver implements Resolve<Post[]> {
    resolve(): Observable<Post[]> {
        return this.store.select('posts_list').filter((x: Post[]|null|undefined) => {
            return !isNullOrUndefined(x) && x.length !== 0;
        }).first();
    }
    constructor(protected store: Store<Post[]>) {}
}
