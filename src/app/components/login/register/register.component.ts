import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    auth: any;
    user: any;
    constructor(protected service: AuthService, protected modal: NgbActiveModal) { }
    ngOnInit() {
        this.auth = this.service.googleAuth;
        this.user = this.service.googleUser;
    }
    clicked_yes(username: string) {
        this.modal.close(username);
    }
    clicked_no() {
        this.modal.dismiss();
    }

}
