import { AfterViewInit, Component, OnInit, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Store } from '@ngrx/store';
import { LOGIN, LOGOUT } from '../../../reducers-effects/login_reducer';
import { AuthService } from '../../../services/auth.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

export declare const gapi: any;

@Component({
  selector: 'app-g-signin',
  templateUrl: './google-auth.component.html',
  styleUrls: ['./google-auth.component.css']
})
export class GoogleAuthComponent implements OnInit {
    auth: any; // GoogleAuth
    user: any; // GoogleUser
    constructor(protected http: Http, protected store: Store<any>, protected service: AuthService,
    protected modal: NgbActiveModal) { }
    ngOnInit() {
        gapi.signin2.render('g-signin', {
            'scope': 'profile email',
            'width': 260,
            'height': 50,
            'longtitle': true,
            'onsuccess': this.g_success.bind(this)
        });
        this.auth = this.service.googleAuth;
        this.user = this.service.googleUser;
    }
    g_success(user) {
        this.store.dispatch({type: LOGIN, payload: {auth: this.user.getAuthResponse().id_token}});
        this.modal.close('success');
    }
    g_login() {
        this.auth.signIn({ux_mode: 'redirect', prompt: 'select_account',
            redirect_uri: 'http://localhost:4200/'});
    }
}
