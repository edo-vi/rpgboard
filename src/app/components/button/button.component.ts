import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { BUTTONSTODISPLAY, GOTOPAGE, NEXT, PREV } from '../../reducers-effects/buttonsreducer';

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
    portion$: Observable<number>;
    buttons$: Observable<number>;
    buttonrange$: Observable<number[]>;
    next$: Observable<boolean>;
    prev$: Observable<boolean>;
    BUTTONSTODISPLAY = BUTTONSTODISPLAY;
    @Input() currentPage$: Observable<number>;
    constructor(protected store: Store<any>) { }

    ngOnInit() {
        this.portion$ = this.store.select('button_state').pluck('portion').pluck('c');
        this.buttons$ = this.store.select('button_state').pluck('total_pages');
        this.buttonrange$ = this.buttons$.map((x: number) =>
            Array(x).fill(1).map((y, i) => i));
        this.next$ = this.store.select('button_state').pluck('portion').map(({c, max}) => {
            return c !== max;
        });
        this.prev$ = this.store.select('button_state').pluck('portion').map(({c, max}) => {
            return c !== 1;
        });
    }
    clicked_page(p: number) {
        this.store.dispatch({type: GOTOPAGE, payload: p+  1});
    }
    clicked_next() {
        this.store.dispatch({type: NEXT});
    }
    clicked_prev() {
        this.store.dispatch({type: PREV});
    }

}
