import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { regions } from '../../../../italy';
import { SELECTEDCOUNTY, SELECTEDREGION } from '../../../reducers-effects/locationreducer';
import { enterAnimation, Region } from '../trova.component';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { GETPOSTS } from '../../../reducers-effects/postsreducer';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css'],
    animations: [enterAnimation]
})
export class SidebarComponent implements OnInit {
    @Input() chosenlocation$: Observable<string[]>;
    @Input() windowsize_: BehaviorSubject<number>;
    cities$: any;
    regions: Region[] = regions;
    ngOnInit() {
        this.chosenlocation$ = this.store.select('chosen_location');
        this.cities$ = this.chosenlocation$.map(x => x[0]).filter(y => y !== null)
            .map(reg => this.regions.filter(v => v.name === reg)[0].counties);
    }
    select_region(region: Region) {
        this.store.dispatch({type: SELECTEDREGION, payload: region});
    }
    select_city(city) {
        this.store.dispatch({type: SELECTEDCOUNTY, payload: city.name});
        this.store.dispatch({type: GETPOSTS, payload: city.name});
    }
    constructor(protected store: Store<any>, protected router: Router) { }
}
