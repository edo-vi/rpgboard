import { Component, OnDestroy, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/publish';
import 'rxjs/add/observable/fromEvent';


import { Store } from '@ngrx/store';
import { REMOVECITY, SELECTEDCOUNTY } from '../../reducers-effects/locationreducer';


import { Actions } from '@ngrx/effects';
import { Subscription } from 'rxjs/Subscription';
import { animate, style, transition, trigger } from '@angular/animations';
import { GETPOSTS, GOTPOSTS, ORDERNEWEST, ORDEROLDEST, Post } from '../../reducers-effects/postsreducer';
import { REMOVEGAME, SELECTEDGAME } from '../../reducers-effects/gamereducer';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { enterFastAnimation } from '../dashboard/dashboard.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostFormComponent } from './post-form/post-form.component';
import { LoginStatus } from '../../reducers-effects/login_reducer';

export interface Region {
    name: string;
    hovering?: boolean;
    counties?: County[];
}

export interface County {
    name: string;
    hovering?: boolean;
}

export const enterAnimation = trigger('enter', [
            transition(':enter', [
                style({opacity: 0}),
                animate('350ms ease')
            ]),
]);

export const enterFastFromRightAnimation = trigger('enterfastfromright', [
        transition(':enter', [
            style({transform: 'translateX(50%)'}),
            animate('150ms ease')
        ]),
        transition(':leave', [
            animate('400ms ease', style({transform: 'translateX(-300%)'}))
        ])
]);

@Component({
    selector: 'app-find',
    templateUrl: './trova.component.html',
    styleUrls: ['./trova.component.css'],
    animations: [enterFastFromRightAnimation, enterAnimation, enterFastAnimation]
})
export class TrovaComponent implements OnInit {
    chosenlocation$: Observable<string[]>;
    chosengame$: Observable<string>;
    authenticated$: Observable<boolean>;
    canpost$: Observable<boolean>;
    loading$: Observable<boolean>;
    windowsize_ = new BehaviorSubject(window.innerWidth);
    posts$: Observable<Post[]>;
    bcontainer: boolean[];
    orderStatus: any;
    obsForAnimation$: Subscription;
    constructor(protected store: Store<Region[]>, protected actions$: Actions,
                protected modal: NgbModal) {}
    ngOnInit() {
        this.chosenlocation$ = this.store.select('chosen_location');
        this.chosengame$ = this.store.select('chosen_game');
        this.authenticated$ = this.store.select('login').pluck('isLogged');
        this.canpost$ = this.store.select('login').pluck('canpost');
        this.store.select('posts_order').subscribe((x: string) => this.orderStatus = x);
        // posts lists
        this.posts$ = this.store.select('posts_list')
            .do((posts: any[]) => this.bcontainer = posts.map(x => false))
            .do(posts => this.obsForAnimation$ = Observable.timer(0, 70)
                .take(this.bcontainer.length)
                .subscribe(x => this.bcontainer[x] = true))
            .combineLatest(this.chosenlocation$, this.chosengame$)
            .map((res) => {
                const posts: any = res[0];
                const location: any = res[1][1];
                const game = res[2];
                if (!location && !game) {
                    return posts;
                } else if (location && !game) {
                    return posts.filter(f => f.location.toLowerCase() === location.toLowerCase());
                } else if (!location && game) {
                    return posts.filter(f => f.game.toLowerCase() === game.toLowerCase());
                } else if (location && game) {
                    return posts.filter(f => {
                        return f.location.toLowerCase() === location.toLowerCase();
                    }).filter((l: Post) => {
                        if (l) {
                            return l.game.toLowerCase() === game.toLowerCase();
                        }
                    });
                }
            });
        this.loading$ = this.actions$.ofType(GETPOSTS, GOTPOSTS).map(x => {
            if (x.type === GETPOSTS) {
                return true;
            } else {return false; }
        });
        window.onresize =  () => this.windowsize_.next(window.innerWidth);
    }
    handle_click_search(event: any) {
        if (event.type === 'city') {
            this.store.dispatch({type: SELECTEDCOUNTY, payload: event.v});
            this.store.dispatch({type: GETPOSTS, payload: event.v});
        } else
            if (event.type === 'game') {
            this.store.dispatch({type: SELECTEDGAME, payload: event.v});
        }
    }
    change_sorting() {
        this.orderStatus === 'newest' ? this.store.dispatch({type: ORDEROLDEST})
                                        :
            this.store.dispatch({type: ORDERNEWEST});
    }
    remove_location() {
        this.store.dispatch({type: REMOVECITY});
    }
    remove_game() {
        this.store.dispatch({type: REMOVEGAME});
    }
    open_modal() {
        this.modal.open(PostFormComponent);
    }
}

@Pipe({name: 'toIsoDate'})
export class ToIsoDatePipe implements PipeTransform {
    transform(v: number) {
        try {
            return new Date(v).toISOString();
        } catch (err) {
            return 'Error';
        }
    }
}
