import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ADDEDPOST, ADDINGPOST } from '../../../reducers-effects/postsreducer';

import { Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { HIDFORM, SHOWFORM } from '../../../reducers-effects/showformreducer';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { searchcities, searchgames } from '../../searchform/searchform.component';
import { regions } from '../../../../italy';



@Component({
    selector: 'app-post-form',
     templateUrl: './post-form.component.html',
     styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
    showForm$: Observable<boolean>;
    chosenlocation: string[];
    chosengame: string;
    cities: any;
    searchgames: any;
    addAd: FormGroup;
    constructor(protected store: Store<any>, protected actions$: Actions,
                public activeModal: NgbActiveModal) { }

    ngOnInit() {
        this.store.select('chosen_location').subscribe((r: string[]) => this.chosenlocation = r);
        this.store.select('chosen_game').subscribe((r: string) => this.chosengame = r);
        this.addAd = new FormGroup({
            game: new FormControl(this.chosengame, [Validators.required, Validators.maxLength(50)]),
            location: new FormControl(this.chosenlocation[1], Validators.required),
            description: new FormControl('', [Validators.required, Validators.maxLength(200)]),
        });
        this.actions$.ofType(ADDEDPOST).subscribe(e => this.hid_form());
        this.showForm$ = this.store.select('show_form');
        this.cities = regions.map(r => r.counties).reduce((a, b) => a.concat(b)).map(v => v.name)
            .sort((a, b) => {
            if (a < b) {
                return -1;
            } else { return 1; }
        });
        this.searchgames = (text$: Observable<string>) => {
            return searchgames(text$, this.store);
        };
    }
    show_form() {
        this.store.dispatch({type: SHOWFORM});
    }
    hid_form() {
        this.activeModal.dismiss('Close');
    }
    submit_ad_form() {
        this.store.dispatch({type: ADDINGPOST, payload: this.addAd.value});
    }

}
