import { Component, Input, OnInit } from '@angular/core';
import { LIKED, Post } from '../../../reducers-effects/postsreducer';
import { enterFastFromRightAnimation } from '../trova.component';
import { LoginStatus } from '../../../reducers-effects/login_reducer';
import { Store } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { Actions } from '@ngrx/effects';
import { Router } from '@angular/router';

@Component({
    selector: 'app-trova-post',
    templateUrl: './trova-post.component.html',
    styleUrls: ['./trova-post.component.css'],
    animations: [enterFastFromRightAnimation]
})
export class TrovaPostComponent implements OnInit {
    @Input() post: Post;
    isOfUser: boolean;
    isLikedByUser: boolean;
    login$: Observable<LoginStatus>;
    canlike$: Observable<boolean>;
    ngOnInit() {
        this.login$ = this.store.select('login');
        this.canlike$ = this.login$.pluck('canlike');
        this.login$.pluck('posts').subscribe( (posts: Post[]) => {
            if (posts) {
                this.isOfUser = posts.map((x: Post) => x._id).indexOf(this.post._id) !== -1;
            } else {
                this.isOfUser = false;
            }
        });
        this.login$.pluck('liked').subscribe((liked: Post[]) => {
            if (liked) {
                this.isLikedByUser = liked.map((x: Post) => x._id).indexOf(this.post._id) !== -1;
            } else {
                this.isLikedByUser = false;
            }
        });
    }
    open_like_modal(content) {
        this.modal.open(content);
    }
    liked(p: Post) {
        this.store.dispatch({type: LIKED, payload: p._id});
    }
    go_to_account(username: string, _id: any) {
        this.router.navigate(['/account', username, {_id: _id}], {queryParamsHandling: null});
    }
    constructor(protected store: Store<LoginStatus>, protected modal: NgbModal
    , protected actions$: Actions, protected router: Router) { }

}
