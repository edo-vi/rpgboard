import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrovaPostComponent } from './trova-post.component';

describe('TrovaPostComponent', () => {
  let component: TrovaPostComponent;
  let fixture: ComponentFixture<TrovaPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrovaPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrovaPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
