import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { SearchformComponent } from '../searchform/searchform.component';
import { routes } from '../../routes';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../../app.module';


describe('AppComponent', () => {
    let fix: ComponentFixture<DashboardComponent>;
    let app: DebugElement;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
      imports: [StoreModule.provideStore(reducer)],
      declarations: [ LoginComponent, HomeComponent, SearchformComponent, DashboardComponent ],
            schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
        fix = TestBed.createComponent(DashboardComponent);
        app = fix.debugElement.componentInstance;
    }));

  it('should create the app', async(() => {
        expect(app).toBeTruthy();
  }));
});
