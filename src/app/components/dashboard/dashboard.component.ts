import { AfterViewInit, Component, OnInit } from '@angular/core';
import 'rxjs/add/observable/interval';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/observable/of';
import { style, animate, transition, trigger, state } from '@angular/animations';

import { LOAD } from '../../reducers-effects/xsrfEffects';
import { NavigationEnd, Router } from '@angular/router';

import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GoogleAuthComponent } from '../login/google-auth/google-auth.component';
import { LOGOUT } from '../../reducers-effects/login_reducer';
import { jumbotronanimation, JumbotronService } from '../../services/jumbotron.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


export const enterFastAnimation = trigger('enterfast', [
        transition(':enter', [
            style({transform: 'translateX(-300%)'}),
            animate('250ms ease')
        ]),
        transition(':leave', [
            animate('350ms ease', style({transform: 'translateX(-300%)'}))
        ])
]);


@Component({
    selector: 'app-root',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
    animations: [enterFastAnimation, jumbotronanimation],
})
export class DashboardComponent implements OnInit, AfterViewInit {
    isLogged$: Observable<boolean>;
    chosenCity$: Observable<string>;
    chosenGame$: Observable<string>;
    username$: Observable<string>;
    jumbostate: BehaviorSubject<string>;
    displaylogin = false;
    constructor(protected store: Store<boolean>, protected router: Router,
                protected modal: NgbModal, protected jumb: JumbotronService) {}
    ngOnInit() {
        this.jumbostate = this.jumb.state_;
        this.isLogged$ = this.store.select('login').pluck('isLogged');
        this.username$ = this.store.select('login').pluck('username');
        this.chosenCity$ = this.store.select('chosen_location');
        this.chosenGame$ =  this.store.select('chosen_game').pluck('title');
        // load csrf token and ads
        this.store.dispatch({type: LOAD});
    }
    ngAfterViewInit() {
        const url = new URL(window.location.href);
        const nextParam = url.searchParams.get('next');
        if (nextParam) {
            setTimeout(() => this.router.navigate(['/' + nextParam]), 600);
        }
    }
    open_login() {
        this.modal.open(GoogleAuthComponent);
    }
    open_logout(logout: any) {
        this.modal.open(logout);
    }
    log_out() {
        this.store.dispatch({type: LOGOUT});
    }
}
