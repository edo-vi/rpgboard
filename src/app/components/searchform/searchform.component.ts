import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/distinctUntilChanged';

import { Subject } from 'rxjs/Subject';


import { Http } from '@angular/http';
import { FINDGAMESNAMES } from '../../reducers-effects/gamereducer';
import { regions } from '../../../italy';
import { enterAnimation, Region } from '../trova/trova.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'app-searchform',
    templateUrl: './searchform.component.html',
    styleUrls: ['./searchform.component.css'],
    animations: [enterAnimation]
})

export class SearchformComponent implements OnInit {
    search_: Subject<{type: string, v: string}> = new Subject<{type: string, v: string}>();
    @Output() selectedInput = new EventEmitter<{type: string, v: string}>();
    searchcities: any;
    searchgames: any;
    @Input() windowsize_: BehaviorSubject<number>;
    constructor(protected store: Store<string>, protected http: Http) { }
    ngOnInit() {
        this.store.dispatch({type: FINDGAMESNAMES});
        this.searchcities = (text$: Observable<string>) => {
            return searchcities(text$);
        };
        this.searchgames = (text$: Observable<string>) => {
            return searchgames(text$, this.store);
        };

    }
    get_cities(input: string) {
        this.search_.next({type: 'cities', v: input});
    }
    get_games(input: string) {
        this.search_.next({type: 'games', v: input});
    }
    emit_city(i: string) {
        this.selectedInput.next({type: 'city', v: i});
    }
    emit_game(i: string) {
        this.selectedInput.next({type: 'game', v: i});
    }
    clear_cities() {
        this.search_.next({type: 'cities', v: 'a'});
    }
    clear_games() {
        this.search_.next({type: 'games', v: 'a'});
    }
}

export const searchcities = (text$: Observable<string>) => {
    return text$.debounceTime(150)
        .distinctUntilChanged()
        .switchMap((v) => {
            if (v.length < 2) {
                return Observable.of([]);
            } else {
                return Observable.of(
                    regions.map((reg: Region) => reg.counties).reduce((a, b) => a.concat(b))
                        .map(x => x.name).filter(n => {
                        const t = new RegExp(v.toLowerCase());
                        return t.test(n.toLowerCase());
                    })
                );
            }
        });
};

export const searchgames = (text$: Observable<string>, store: Store<any>) => {
    return text$.debounceTime(150)
        .distinctUntilChanged()
        .switchMap( (v) => {
            if (v.length < 3) {return Observable.of([]); } else {
                return store.select('games_names_list').map((n: string[]) => {
                    const t = new RegExp(v.toLowerCase());
                    return n.filter(g => t.test(g.toLowerCase())).slice(0, 15);
                });
            }
        });
};
