import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SearchformComponent } from './searchform.component';
import { DashboardComponent } from '../dashboard/dashboard.component';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { reducer } from '../../app.module';
import { Store, StoreModule } from '@ngrx/store';
import 'rxjs/add/operator/filter';

describe('SearchformComponent', () => {
    let component: SearchformComponent;
    let fix: ComponentFixture<SearchformComponent>;
    let store: Store<any>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
          imports: [StoreModule.provideStore(reducer)],
          declarations: [ DashboardComponent, SearchformComponent, HomeComponent, LoginComponent ],
          schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fix = TestBed.createComponent(SearchformComponent);
        component = fix.componentInstance;
        store = TestBed.get(Store);
    });
    it('should correctly dispatch a SEARCHED to change search state', async(() => {
        component.search('Roma');
        store.select('search').filter(x => x !== '').subscribe(x => expect(x).toEqual('Roma'));
    }));
    it('should only fires an event when input is not null', async(() => {
        component.search('first');
        component.search('');
        store.select('search').subscribe(x => expect(x).not.toEqual(''));
    }));
});
