import { Component, OnInit } from '@angular/core';
import { LoginStatus } from '../../reducers-effects/login_reducer';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { enterAnimation } from '../trova/trova.component';
import { Post } from '../../reducers-effects/postsreducer';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
    animations: [enterAnimation]
})
export class UserComponent implements OnInit {
    user$: Observable<LoginStatus>;
    userposts: Post[];
    likedposts: Post[];
    whatToLoad = 'account';
    constructor(protected store: Store<LoginStatus>) { }

    ngOnInit() {
        this.user$ = this.store.select('login').do((x: LoginStatus) => {
            this.userposts = x.posts.sort((a, b) => b.timestamp - a.timestamp);
            this.likedposts = x.liked.sort((a, b) => b.timestamp - a.timestamp);
        });
    }

}
