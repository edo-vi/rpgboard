import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { enterAnimation } from '../../trova/trova.component';
import { LoginStatus } from '../../../reducers-effects/login_reducer';
import { Post } from '../../../reducers-effects/postsreducer';
import { ActivatedRoute } from '@angular/router';
import { GETACCOUNTINFO, GOTACCOUNTINFO } from '../../../reducers-effects/accountreducer';
import { Actions } from '@ngrx/effects';


@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css'],
    animations: [enterAnimation]
})
export class AccountComponent implements OnInit {
    loading = true;
    account$: Observable<Account>;
    whatToLoad = 'account';
    constructor(protected store: Store<LoginStatus>, protected route: ActivatedRoute,
                protected actions$: Actions) { }
    ngOnInit() {
        Observable.of(1).withLatestFrom(this.route.params.pluck('_id'), this.store.select('login').pluck('jwt'), (a, b, c) => [b, c])
            .subscribe(([pid , jwt]) => this.store.dispatch({type: GETACCOUNTINFO, payload: {pid: pid, jwt: jwt}}));
        this.account$ = this.store.select('account').filter(x => x !== null);
        this.actions$.ofType(GOTACCOUNTINFO).subscribe(x => this.loading = false);
    }

}
