import { AfterViewInit, Component, OnInit } from '@angular/core';
import { cat } from '../../../cat';
import { enterAnimation } from '../trova/trova.component';
import { Store } from '@ngrx/store';
import { CLEARCATEGORY, FOUNDGAMES, SELECTEDCATEGORY, SELECTEDGAME } from '../../reducers-effects/gamereducer';
import { Observable } from 'rxjs/Observable';
import { Actions } from '@ngrx/effects';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/pairwise';

export interface Game {
    name: string;
    owners: number;
    ratings: number;
    avg: number;
}

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.css'],
    animations: [enterAnimation]
})
export class GamesComponent implements OnInit, AfterViewInit {
    show$: Observable<boolean>;
    loading$: Observable<boolean>;
    games$: Observable<Game[]>;
    currentPage$: Observable<number>;
    category$: Observable<{en: string, it: string}>;
    numberPages: number;
    windowsize_ = new BehaviorSubject(window.innerWidth);

    cat = cat;
    constructor(protected store: Store<any>, protected actions$: Actions) { }
    ngOnInit() {
        this.games$ = this.store.select('games_list');
        this.currentPage$ = this.store.select('button_state').pluck('current_page');
        this.show$ = this.store.select('chosen_category').map(({en, it}) => en === null || it === null);
        this.category$ = this.store.select('chosen_category');
        this.loading$ = this.actions$.ofType(SELECTEDCATEGORY, FOUNDGAMES)
            .map(r => {
                if (r.type === SELECTEDCATEGORY) {
                    return true;
                } else {
                    return false;
                }
            });
        window.onresize =  () => this.windowsize_.next(window.innerWidth);
    }
    ngAfterViewInit() {
        // resetting columns and grid
        this.windowsize_.pairwise().subscribe(([p, c]) => {
            const col1 = document.getElementById('col1') as HTMLElement;
            const col2 = document.getElementById('col2') as HTMLElement;
            const col3 = document.getElementById('col3') as HTMLElement;
            const col4 = document.getElementById('col4') as HTMLElement;
            // from two-column to one-column || from four-column to one-column
            if ((c < 380 && (p >= 380 && p < 720)) || (c < 380 && p >= 720)) {
                col1.style.display = 'block';
                col2.style.display = 'none';
                col3.style.display = 'none';
                col4.style.display = 'none';
            } // from one-column to two-column || from four-column to two-column
            else if (((c >= 380 && c < 720) && p < 380 ) || ((p >= 720) && (c < 720 && c >= 380)) ) {
                col1.style.display = 'block';
                col2.style.display = 'block';
                col3.style.display = 'none';
                col4.style.display = 'none';
            } // from one-column to four-column || two-column to four-column
            else if ((c >= 720) && p < 720) {
                col1.style.display = 'block';
                col2.style.display = 'block';
                col3.style.display = 'block';
                col4.style.display = 'block';
            }
        });
        Observable.fromEvent(document.getElementById('btncol'), 'click')
            .withLatestFrom(this.windowsize_, (a, b) => b)
            .catch(e => Observable.of(e))
            .subscribe(s => {
                if (s < 380) {
                    const col1 = document.getElementById('col1') as HTMLElement;
                    const col2 = document.getElementById('col2') as HTMLElement;
                    const col3 = document.getElementById('col3') as HTMLElement;
                    const col4 = document.getElementById('col4') as HTMLElement;
                    const a = [col1, col2, col3, col4];
                    const n = a.map(x => x.style.display !== 'none').indexOf(true);
                    a[n % 4].style.display = 'none';
                    a[(n + 1) % 4].style.display = 'block';
                }
                if (s < 720 && s >= 380) {
                    const col1 = document.getElementById('col1') as HTMLElement;
                    const col2 = document.getElementById('col2') as HTMLElement;
                    const col3 = document.getElementById('col3') as HTMLElement;
                    const col4 = document.getElementById('col4') as HTMLElement;
                    const a = [col1, col2, col3, col4];
                    const n = [];
                    for (const u of a) {
                        if (u.style.display !== 'none') {n.push(a.indexOf(u)); }
                    }
                    a[(n[0]) % 4].style.display = 'none';
                    a[(n[1]) % 4].style.display = 'none';
                    a[(n[0] + 2) % 4].style.display = 'block';
                    a[(n[1] + 2) % 4].style.display = 'block';
                }
        });
    }
    select_category(cat: any) {
        this.store.dispatch({type: SELECTEDCATEGORY, payload: {en: cat.name, it: cat.it}});
    }
    select_game(g: any) {
        this.store.dispatch({type: SELECTEDGAME, payload: g.name});
    }
    remove_location() {
        this.store.dispatch({type: CLEARCATEGORY});
    }
}
