import { Component, OnDestroy, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/timer';
import { Observable } from 'rxjs/Observable';
import { enterFastAnimation } from '../dashboard/dashboard.component';


const hoveringLessScaleAnimation = trigger('hovering', [
        state('true', style({transform: 'scale(1.05)', backgroundColor: 'rgba(255, 255, 255, 0.2)'})),
        state('false', style({transform: 'scale(1)', backgroundColor: 'rgba(255, 255, 255, 0.1)'})),
        transition('* <=> *', animate('100ms ease'))
        ]);

export const enterFastAnimationfromTop = trigger('enterfastfromtop', [
        transition('void => *', [
            style({transform: 'translateY(-300%)'}),
            animate('250ms ease')
        ]),
]);

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    animations: [enterFastAnimation, hoveringLessScaleAnimation]
})
export class HomeComponent implements OnInit, OnDestroy {
    statebtn = true;
    ifs = [false, false, false];
    constructor() { }
    ngOnInit() {
        Observable.timer(0, 70).take(3).subscribe(y => this.ifs[y] = true);
    }
    ngOnDestroy() {
    }
}
