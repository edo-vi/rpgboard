import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { routes } from '../../routes';
import { LoginComponent } from '../login/login.component';
import { SearchformComponent } from '../searchform/searchform.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';



describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [ LoginComponent, SearchformComponent, HomeComponent, DashboardComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
