import { RegisterPage } from './app.po';

describe('Signup Page', () => {
      let page: RegisterPage;
      const undefinedBorder = '1px solid rgb(204, 204, 204)';
      const greenBorder = '2px solid rgb(160, 187, 160)';
      const redBorder = '2px solid rgb(218, 74, 71)';

      beforeEach(() => {
         page = new RegisterPage();
         page.navigateTo();
      });

      it('username field\'s border is undefined at first ', () => {
        expect(page.getUsernameBorder()).toEqual(undefinedBorder);
      });
      it('username border is green after typing', () => {
        page.typeInField('username', 'k');
        expect(page.getUsernameBorder()).toEqual(greenBorder);
      });
      it('password is red if less than 8chars are typed, and green if more', () => {
          page.typeInField('password', 'settele');
          expect(page.getPasswordBorder).toBe(redBorder);
          page.typeInField('username', 'morethan7chars');
          expect(page.getPasswordBorder).toBe(greenBorder);
      });

});
