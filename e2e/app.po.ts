import { browser, by, element } from 'protractor';

export class RegisterPage {
  navigateTo() {
    return browser.get('/signup');
  }

    getUsernameBorder() {
      return element(by.css('.form-control[name=username]')).getCssValue('border-bottom');
      }
    getPasswordBorder() {
      return element(by.css('.form-control[name=password]')).getCssValue('border-bottom');
    }
    getCpasswordBorder() {
      return element(by.css('.form-control[name=cpassword]')).getCssValue('border-bottom');
    }
    typeInField(fieldName: string, val: string) {
      return element(by.css(`.form-control[name=${fieldName}]`)).clear().then(() => {
        return element(by.css(`.form-control[name=${fieldName}]`)).sendKeys(val);
      });
    }
}
